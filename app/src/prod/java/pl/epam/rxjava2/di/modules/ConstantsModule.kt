package pl.epam.rxjava2.di.modules

import dagger.Module
import dagger.Provides
import pl.epam.rxjava2.di.DiConstants
import javax.inject.Named

@Module
class ConstantsModule {

    @Provides
    @Named(DiConstants.API_URL)
    internal fun provideApiUrl(): String {
        return "https://www.epam.com"
    }

}
package pl.epam.rxjava2.api

import pl.epam.rxjava2.app.Settings
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.models.Article
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.models.Invitation
import pl.epam.rxjava2.models.Skill
import pl.epam.rxjava2.models.SkillCategory
import kotlin.math.min

/**
 * Created by Lukasz Szymanski.
 */
object StubData {

    var skills: MutableList<Skill> = mutableListOf(
            Skill(101, "Kotlin", 0),
            Skill(102, "Python", 0),
            Skill(103, "C++",0),
            Skill(104, "Teamwork", 1),
            Skill(105, "Positive attitude", 1),
            Skill(106, "Responsibility", 1),
            Skill(107, "Android", 2),
            Skill(108, "iOS", 2),
            Skill(109, "Windows", 2)
    )

    var skillCategories: MutableList<SkillCategory> = mutableListOf(
            SkillCategory(0, "Programming languages"),
            SkillCategory(1, "Soft skills"),
            SkillCategory(2, "Operation systems")
    )

    var currentAppUser: AppUser = AppUser("1", "john.doe@gmail.com", "John", "Doe", "111222333", "Cracow", 25,
            mutableListOf(skills[0].copy(isMain = true),
                    skills[3]))

    var invitationsReceived: MutableList<AppUser> = mutableListOf(
            AppUser("101","alice.red@gmail.com", "Alice", "Red", "123456789", "Cracow", 24,
                    mutableListOf(skills[0].copy(isMain = true),
                            skills[6],
                            skills[7])),
            AppUser("102", "bob.green@gmail.com", "Bob", "Green", "987654321", "Modlniczka", 26,
                    mutableListOf(skills[0].copy(isMain = true),
                            skills[7],
                            skills[8]))
    )

    var invitationsSent: MutableList<AppUser> = mutableListOf(
            AppUser("201", "mark.johnson@gmail.com", "Mark", "Johnson", "135792468", "Brzesko", 25,
                    mutableListOf(skills[1].copy(isMain = true),
                            skills[6]))
    )

    var myExperts: MutableList<AppUser> = mutableListOf(
            AppUser("301", "rob.walker@gmail.com", "Rob", "Walker", "657843901", "Warsaw", 27,
                    mutableListOf(skills[0].copy(isMain = true),
                            skills[3],
                            skills[4])),
            AppUser("302", "susan.smith@gmail.com", "Susan", "Smith", "129034875", "Cracow", 27,
                    mutableListOf(skills[0].copy(isMain = true),
                            skills[4],
                            skills[5])),
            AppUser("303", "daniel.blue@gmail.com", "Daniel", "Blue", "223334444", "Katowice", 27,
                    mutableListOf(skills[0].copy(isMain = true),
                            skills[5]))
    ).plus(createExperts(600, 25, 45)).toMutableList()

    var otherUsers: MutableList<AppUser> = listOf(
            AppUser("401", "ben.aaa@gmail.com", "Ben", "Aaa", "111111111", "Wieliczka", 29,
                    mutableListOf(skills[6].copy(isMain = true),
                            skills[3])),
            AppUser("402", "max.bbb@gmail.com", "Max", "Bbb", "222222222", "Cracow", 22,
                    mutableListOf(skills[7].copy(isMain = true),
                            skills[4])),
            AppUser("403", "brian.ccc@gmail.com", "Brian", "Ccc", "333333333", "Cracow", 21,
                    mutableListOf(skills[8].copy(isMain = true),
                            skills[5]))
    ).plus(createOtherUsers(404, 18, 60)).toMutableList()

    var reportedUsers: MutableList<AppUser> = mutableListOf()

    var blockedUsers: MutableList<AppUser> = mutableListOf()

    var articles: MutableList<Article> = mutableListOf(
            Article("Welcoming Android 8.1 Oreo and Android Oreo (Go edition)",
                    "https://2.bp.blogspot.com/-sLGydlfK8AM/WibSkBxHOlI/AAAAAAAAE20/hewUiVpk-uYb8YvRjbcGXJzrNh-twjH5gCLcBGAs/s1600/image1.png",
                    "https://android-developers.googleblog.com/2017/12/welcoming-android-81-oreo-and-android.html",
                    "Android Developers Blog"),
            Article("Huawei P20 likely to be revealed in Paris on March 27",
                    "https://cdn57.androidauthority.net/wp-content/uploads/2017/03/huawei-logo-aa-gds-mwc17-840x560.jpg",
                    "https://www.androidauthority.com/huawei-p20-likely-revealed-paris-march-27-834367/",
                    "ANDROID AUTHORITY"),
            Article("Announcing Architecture Components 1.0 Stable",
                    "https://4.bp.blogspot.com/-iRzg0p4Y2Vk/WRzePUb2TqI/AAAAAAAAEME/KmIkplhQJL4WL592-YSJanocRgiMu3MqgCLcB/s1600/Screen%2BShot%2B2017-05-17%2Bat%2B4.33.14%2BPM.png",
                    "https://android-developers.googleblog.com/2017/11/announcing-architecture-components-10.html",
                    "Android Developers Blog"),
            Article("Windows 10 help, tips and tricks",
                    "https://www.windowscentral.com/sites/wpcentral.com/files/topic_images/2016/windows-10.png",
                    "https://www.windowscentral.com/windows-10-help",
                    "Windows Central")
    )

    fun getAllUsers(): MutableList<AppUser> {
        val list: MutableList<AppUser> = mutableListOf(currentAppUser)
        list.addAll(invitationsReceived)
        list.addAll(invitationsSent)
        list.addAll(myExperts)
        list.addAll(otherUsers)
        list.addAll(blockedUsers)
        // reported users are not removed from lists above so we do not add them here

        return list
    }

    fun getUserProfile(userGuid: String): AppUser {
        return getAllUsers().filter { it.guid == userGuid }.first()
    }

    fun createInvitation(appUser: AppUser) : Invitation =
            Invitation(appUser.guid,
                    appUser.guid,
                    appUser.firstName,
                    appUser.lastName,
                    appUser.city,
                    appUser.age)

    fun createBaseUser(appUser: AppUser) : BaseUser =
            BaseUser(appUser.guid,
                    appUser.firstName,
                    appUser.lastName,
                    appUser.city,
                    appUser.age)

    fun getMoreUsers(
            minAge: Int,
            maxAge: Int,
            amountOfUsers: Int,
            lastUserGuid: String?
    ): MutableList<AppUser> {
        val filteredUsers = otherUsers
                .filter { it.age in minAge..maxAge }

        val lastUserIndex = filteredUsers.indexOfFirst { it.guid == lastUserGuid }

        if (lastUserGuid != Settings.EMPTY_GUID && lastUserIndex == -1)
            return mutableListOf()

        val startIndex = lastUserIndex + 1
        val stopIndex = min(startIndex + amountOfUsers, filteredUsers.size)

        if (startIndex < filteredUsers.size && stopIndex < filteredUsers.size + 1 && startIndex < stopIndex)
            return filteredUsers.subList(startIndex, stopIndex).toMutableList()

        return mutableListOf()
    }

    fun getMoreExperts(
            amountOfUsers: Int,
            lastUserGuid: String?
    ): MutableList<AppUser> {

        val lastUserIndex = myExperts.indexOfFirst { it.guid == lastUserGuid }

        if (lastUserGuid != Settings.EMPTY_GUID && lastUserIndex == -1)
            return mutableListOf()

        val startIndex = lastUserIndex + 1
        val stopIndex = min(startIndex + amountOfUsers, myExperts.size)

        if (startIndex < myExperts.size && stopIndex < myExperts.size + 1 && startIndex < stopIndex)
            return myExperts.subList(startIndex, stopIndex).toMutableList()

        return mutableListOf()
    }

    private fun createOtherUsers(
            startUserGuid: Int,
            minAge: Int,
            maxAge: Int
    ): MutableList<AppUser> = MutableList(100, { index ->
        val guid = startUserGuid + index
        AppUser("$guid",
                "user_$guid@gmail.com",
                "firstName_$guid",
                "lastName_$guid",
                "$guid$guid$guid",
                "city $guid",
                index % (maxAge + 1 - minAge) + minAge
        )
    })

    private fun createExperts(
            startUserGuid: Int,
            minAge: Int,
            maxAge: Int
    ): MutableList<AppUser> = MutableList(30, { index ->
        val guid = startUserGuid + index
        AppUser("$guid",
                "expert_$guid@gmail.com",
                "expert",
                "$guid",
                "$guid$guid$guid",
                "city $guid",
                index % (maxAge + 1 - minAge) + minAge
        )
    })
}
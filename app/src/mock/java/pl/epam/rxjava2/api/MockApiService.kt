package pl.epam.rxjava2.api

import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import okhttp3.MediaType
import okhttp3.ResponseBody
import pl.epam.rxjava2.R
import pl.epam.rxjava2.api.response.ServerErrorResponse
import pl.epam.rxjava2.app.App
import pl.epam.rxjava2.app.prefs.SharedPreferencesHelper
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.models.Article
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.models.Invitation
import pl.epam.rxjava2.models.Skill
import pl.epam.rxjava2.models.SkillCategory
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.Calls
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import java.util.Random
import java.util.concurrent.TimeUnit


/**
 * Created by Lukasz Szymanski.
 */

class MockApiService protected constructor(
        private val context: App,
        sharedPreferencesHelper: SharedPreferencesHelper,
        apiUrl: String
): ApiController(context, sharedPreferencesHelper, apiUrl), ApiService {

    private val behaviorDelegate: BehaviorDelegate<ApiService>

    init {

        val retrofit = Retrofit.Builder()
                .baseUrl(apiUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        val behavior = NetworkBehavior.create()
        val mockRetrofit = MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build()
        behaviorDelegate = mockRetrofit.create(ApiService::class.java)

        behavior.setDelay(500, TimeUnit.MILLISECONDS)
        behavior.setFailurePercent(0);
    }

//Articles
    override fun getArticles(): Single<MutableList<Article>> {
        return behaviorDelegate
                .returningResponse(StubData.articles)
                .getArticles()
    }

//Blocked
    override fun getBlockedUsersList(): Single<MutableList<BaseUser>> {
        return behaviorDelegate
                .returningResponse(StubData.blockedUsers
                        .map { StubData.createBaseUser(it) }
                        .toMutableList())
                .getBlockedUsersList()
    }

    override fun blockUser(userGuid: String): Completable {
        val blockedUser = StubData.getUserProfile(userGuid)

        if (StubData.blockedUsers.contains(blockedUser)) {
            return behaviorDelegate
                    .returning(Calls.response(createErrorResponse(404, "This user was already blocked!")))
                    .blockUser(userGuid)
        }

        StubData.invitationsSent.remove(blockedUser)
        StubData.invitationsReceived.remove(blockedUser)
        StubData.myExperts.remove(blockedUser)
        StubData.otherUsers.remove(blockedUser)
        StubData.blockedUsers.add(blockedUser)

        return behaviorDelegate
                .returningResponse(Unit)
                .blockUser(userGuid)
    }

    override fun unblockUser(userGuid: String): Completable {
        val unblockedUser = StubData.getUserProfile(userGuid)
        if (!StubData.blockedUsers.contains(unblockedUser)) {
            return behaviorDelegate
                    .returning(Calls.response(createErrorResponse(404, "This user was not blocked!")))
                    .unblockUser(userGuid)
        }

        StubData.blockedUsers.remove(unblockedUser)
        StubData.otherUsers.add(unblockedUser)

        return behaviorDelegate
                .returningResponse(Unit)
                .unblockUser(userGuid)
    }

//Experts
    override fun getMyExpertsList(
        amountOfUsers: Int,
        lastUserGuid: String
    ): Flowable<MutableList<BaseUser>> {

        return behaviorDelegate
                .returningResponse(StubData.getMoreExperts(amountOfUsers, lastUserGuid)
                        .map { StubData.createBaseUser(it) }
                        .toMutableList())
                .getMyExpertsList(amountOfUsers, lastUserGuid)
    }

    override fun removeMyExpert(userGuid: String): Completable {
        val user = StubData.getUserProfile(userGuid)

        if (!StubData.myExperts.contains(user)) {
            return behaviorDelegate
                    .returning(Calls.response(createErrorResponse(404, "This user is not on your experts list!")))
                    .removeMyExpert(userGuid)
        }

        StubData.myExperts.remove(user)
        StubData.otherUsers.add(user)

        return behaviorDelegate
                .returningResponse(Unit)
                .removeMyExpert(userGuid)
    }

    override fun searchExperts(
            minAge: Int,
            maxAge: Int,
            amountOfUsers: Int,
            lastUserGuid: String
    ): Single<MutableList<BaseUser>> {

        return behaviorDelegate
                .returningResponse(StubData.getMoreUsers(minAge, maxAge, amountOfUsers, lastUserGuid)
                        .map { StubData.createBaseUser(it) }
                        .toMutableList())
                .searchExperts(minAge, maxAge, amountOfUsers, lastUserGuid)
    }

    override fun rateExpert(userGuid: String, agreement: Boolean, opinion: String): Maybe<String> {
        val random = Random()
        val response: Any = if (random.nextBoolean()) context.getString(R.string.rate_opinion_example) else Unit
        return behaviorDelegate
                .returningResponse(response)
                .rateExpert(userGuid, agreement, opinion)
    }

//Invitations
    override fun acceptInvitation(invitationGuid: String): Completable {
        val user = StubData.getUserProfile(invitationGuid)

        if (StubData.myExperts.contains(user)) {
            return behaviorDelegate
                    .returning(Calls.response(createErrorResponse(404, "The user is already on your experts list!")))
                    .acceptInvitation(invitationGuid)
        } else if (!StubData.invitationsReceived.contains(user)) {
            return behaviorDelegate
                    .returning(Calls.response(createErrorResponse(404, "The invitation is no longer available!")))
                    .acceptInvitation(invitationGuid)
        }

        StubData.invitationsReceived.remove(user)
        StubData.myExperts.add(user)

        return behaviorDelegate
                .returningResponse(Unit)
                .acceptInvitation(invitationGuid)
    }

    override fun cancelInvitation(invitationGuid: String): Completable {
        val user = StubData.getUserProfile(invitationGuid)

        if (!StubData.invitationsSent.contains(user)) {
            return behaviorDelegate
                    .returning(Calls.response(createErrorResponse(404, "The invitation is no longer available!")))
                    .cancelInvitation(invitationGuid)
        }

        StubData.invitationsSent.remove(user)
        StubData.otherUsers.add(user)

        return behaviorDelegate
                .returningResponse(Unit)
                .cancelInvitation(invitationGuid)
    }

    override fun sendInvitation(userGuid: String): Completable {
        val invitedUser = StubData.getUserProfile(userGuid)

        if (StubData.invitationsSent.contains(invitedUser)) {
            return behaviorDelegate
                    .returning(Calls.response(createErrorResponse(404, "The invitation was sent already!")))
                    .sendInvitation(userGuid)
        }

        StubData.otherUsers.remove(invitedUser)
        StubData.blockedUsers.remove(invitedUser)
        StubData.invitationsSent.add(invitedUser)

        return behaviorDelegate
                .returningResponse(Unit)
                .sendInvitation(userGuid)
    }

    override fun rejectInvitation(invitationGuid: String): Completable {
        val user = StubData.getUserProfile(invitationGuid)

        if (!StubData.invitationsReceived.contains(user)) {
            return behaviorDelegate
                    .returning(Calls.response(createErrorResponse(404, "The invitation is no longer available!")))
                    .rejectInvitation(invitationGuid)
        }

        StubData.invitationsReceived.remove(user)
        StubData.otherUsers.add(user)

        return behaviorDelegate
                .returningResponse(Unit)
                .rejectInvitation(invitationGuid)
    }

    override fun getInvitationsReceived(): Single<MutableList<Invitation>> {
        return behaviorDelegate
                .returningResponse(StubData.invitationsReceived
                        .map { StubData.createInvitation(it) }
                        .toMutableList())
                .getInvitationsReceived()
    }

    override fun getInvitationsSent(): Single<MutableList<Invitation>> {
        return behaviorDelegate
                .returningResponse(StubData.invitationsSent
                        .map { StubData.createInvitation(it) }
                        .toMutableList())
                .getInvitationsSent()
    }

//Report
    override fun reportUser(userGuid: String, reason: String): Completable {
        val reportedUser = StubData.getUserProfile(userGuid)
        if (StubData.reportedUsers.contains(reportedUser)) {
            return behaviorDelegate
                    .returning(Calls.response(createErrorResponse(404, "This user was already reported!")))
                    .reportUser(userGuid, reason)
        }

        StubData.reportedUsers.add(reportedUser)

        return behaviorDelegate
                .returningResponse(Unit)
                .reportUser(userGuid, reason)
    }

//Skills
    override fun getAllSkills(): Single<MutableList<Skill>> {
        return behaviorDelegate
                .returningResponse(StubData.skills)
                .getAllSkills()
    }

    override fun getAllSkillCategories(): Single<MutableList<SkillCategory>> {
        return behaviorDelegate
                .returningResponse(StubData.skillCategories)
                .getAllSkillCategories()
    }

//User
    override fun getUserProfile(): Single<AppUser> {
        return behaviorDelegate
                .returningResponse(StubData.currentAppUser)
                .getUserProfile()
    }

    override fun getUserProfile(userGuid: String): Single<AppUser> {
        var appUser = StubData.getUserProfile(userGuid)
        if (!StubData.myExperts.contains(appUser))
            appUser = appUser.copy(email="", phone = "")
        return behaviorDelegate
                .returningResponse(appUser)
                .getUserProfile(userGuid)
    }

    override fun updateUserProfile(appUser: AppUser): Single<AppUser> {
        StubData.currentAppUser = appUser.copy(guid=StubData.currentAppUser.guid)

        return behaviorDelegate
                .returningResponse(StubData.currentAppUser)
                .updateUserProfile(appUser)
    }

    //------------------------------------------------------------------------

    private fun createErrorResponse(code: Int, message: String?) : Response<ServerErrorResponse> =
        Response.error(
                code,
                ResponseBody.create(
                        MediaType.parse("application/json"),
                        Gson().toJson(ServerErrorResponse(message))))

    companion object {

        private val LOG_TAG = MockApiService::class.java.simpleName

        private var instance: MockApiService? = null

        fun getInstance(
                context: App,
                sharedPreferencesHelper: SharedPreferencesHelper,
                apiUrl: String): MockApiService {

            if (instance == null) {
                instance = MockApiService(context, sharedPreferencesHelper, apiUrl)
            }

            return instance!!
        }
    }
}
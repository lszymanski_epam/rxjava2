package pl.epam.rxjava2.di.modules

import dagger.Module
import dagger.Provides
import pl.epam.rxjava2.api.ApiController
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.api.MockApiService
import pl.epam.rxjava2.app.App
import pl.epam.rxjava2.app.prefs.SharedPreferencesHelper
import pl.epam.rxjava2.di.DiConstants
import pl.epam.rxjava2.di.scopes.AppScope
import javax.inject.Named

/**
 * Created by Lukasz Szymanski.
 */

@Module
class ApiModule {

    @Provides
    @AppScope
    internal fun provideApiController(
            context: App,
            sharedPreferencesHelper: SharedPreferencesHelper,
            @Named(DiConstants.API_URL) apiUrl: String): ApiController {

        return MockApiService.getInstance(context, sharedPreferencesHelper, apiUrl)
    }

    @Provides
    @AppScope
    internal fun provideApiService(apiController: ApiController): ApiService {
        return apiController as MockApiService
    }
}

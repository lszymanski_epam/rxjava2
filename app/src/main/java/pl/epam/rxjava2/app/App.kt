package pl.epam.rxjava2.app


import android.support.multidex.MultiDexApplication
import pl.epam.rxjava2.di.components.AppComponent
import pl.epam.rxjava2.di.components.DaggerAppComponent
import pl.epam.rxjava2.di.modules.ApiModule
import pl.epam.rxjava2.di.modules.AppModule
import pl.epam.rxjava2.di.modules.ConstantsModule
import pl.epam.rxjava2.di.modules.PrefsModule
import pl.epam.rxjava2.di.modules.RepositoriesModule

class App : MultiDexApplication() {

    var appComponent: AppComponent? = null
        private set

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .constantsModule(ConstantsModule())
                .apiModule(ApiModule())
                .prefsModule(PrefsModule())
                .repositoriesModule(RepositoriesModule())
                .build()
    }
}

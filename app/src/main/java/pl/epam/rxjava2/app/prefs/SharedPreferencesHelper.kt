package pl.epam.rxjava2.app.prefs


import android.content.SharedPreferences

class SharedPreferencesHelper(private val prefs: SharedPreferences) {

    private val editor: SharedPreferences.Editor
        get() = prefs.edit()
}

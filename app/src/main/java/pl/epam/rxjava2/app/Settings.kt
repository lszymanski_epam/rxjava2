package pl.epam.rxjava2.app


object Settings {

    const val COMPANY_URL = "https://www.epam.com/"
    const val EMPTY_GUID = "00000000-0000-0000-0000-000000000000"
    const val DEFAULT_AMOUNT_OF_USERS = 10

}

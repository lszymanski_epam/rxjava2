package pl.epam.rxjava2.api

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.epam.rxjava2.BuildConfig
import pl.epam.rxjava2.app.App
import pl.epam.rxjava2.app.prefs.SharedPreferencesHelper
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Lukasz Szymanski.
 */

open class ApiController protected constructor(context: App, sharedPreferencesHelper: SharedPreferencesHelper, apiUrl: String) {

    var apiService: ApiService? = null

    init {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        val httpClient = OkHttpClient.Builder()
                .addInterceptor { chain ->
                    val builder = chain.request().newBuilder()
                            .addHeader("Content-Type", "application/JSON")
                    val request = builder.build()
                    chain.proceed(request)
                }
                .addInterceptor(loggingInterceptor)
                .connectTimeout(CONNECTION_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .build()

        val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl(apiUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    companion object {

        private val LOG_TAG = ApiController::class.java.simpleName

        private val CONNECTION_TIMEOUT_SECONDS: Long = 10
        private val WRITE_TIMEOUT_SECONDS: Long = 10
        private val READ_TIMEOUT_SECONDS: Long = 30

        private var instance: ApiController? = null

        fun getInstance(context: App, sharedPreferencesHelper: SharedPreferencesHelper, apiUrl: String): ApiController {
            if (instance == null) {
                instance = ApiController(context, sharedPreferencesHelper, apiUrl)
            }
            return instance!!
        }
    }
}

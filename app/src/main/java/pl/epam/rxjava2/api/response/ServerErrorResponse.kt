package pl.epam.rxjava2.api.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Lukasz Szymanski.
 */

data class ServerErrorResponse(

        @SerializedName("Message")
        val error: String? = null

)

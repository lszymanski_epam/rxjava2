package pl.epam.rxjava2.api

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import pl.epam.rxjava2.constants.RestMethodNames
import pl.epam.rxjava2.constants.RestMethodParams
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.models.Article
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.models.Invitation
import pl.epam.rxjava2.models.Skill
import pl.epam.rxjava2.models.SkillCategory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Created by Lukasz Szymanski.
 */

interface ApiService {

//Articles
    @GET(RestMethodNames.ARTICLES)
    fun getArticles(): Single<MutableList<Article>>

//Blocked
    @GET(RestMethodNames.USER_BLOCKED_LIST)
    fun getBlockedUsersList(): Single<MutableList<BaseUser>>

    @POST(RestMethodNames.USER_BLOCK)
    fun blockUser(
            @Path(RestMethodParams.USER_ID) userGuid: String
    ): Completable

    @POST(RestMethodNames.USER_UNBLOCK)
    fun unblockUser(
            @Path(RestMethodParams.USER_ID) userGuid: String
    ): Completable

//Experts
    @GET(RestMethodNames.EXPERTS_LIST)
    fun getMyExpertsList(
        @Path(RestMethodParams.AMOUNT_OF_USERS) amountOfUsers: Int,
        @Path(RestMethodParams.LAST_USER_GUID) lastUserGuid: String
    ): Flowable<MutableList<BaseUser>>

    @POST(RestMethodNames.EXPERTS_REMOVE)
    fun removeMyExpert(
            @Path(RestMethodParams.USER_ID) userGuid: String
    ): Completable

    @GET(RestMethodNames.EXPERTS_SEARCH)
    fun searchExperts(
            @Path(RestMethodParams.MIN_AGE) minAge: Int,
            @Path(RestMethodParams.MAX_AGE) maxAge: Int,
            @Path(RestMethodParams.AMOUNT_OF_USERS) amountOfUsers: Int,
            @Path(RestMethodParams.LAST_USER_GUID) lastUserGuid: String
    ): Single<MutableList<BaseUser>>

    @POST(RestMethodNames.EXPERTS_RATE)
    fun rateExpert(
            @Path(RestMethodParams.USER_ID) userGuid: String,
            @Path(RestMethodParams.AGREEMENT) agreement: Boolean,
            @Body opinion: String
    ): Maybe<String>

//Skills
    @GET(RestMethodNames.SKILLS_ALL)
    fun getAllSkills(): Single<MutableList<Skill>>

    @GET(RestMethodNames.SKILLS_CATEGORIES_ALL)
    fun getAllSkillCategories(): Single<MutableList<SkillCategory>>

//Invitations
    @POST(RestMethodNames.INVITATIONS_ACCEPT)
    fun acceptInvitation(
            @Path(RestMethodParams.INVITATION_ID) invitationGuid: String
    ): Completable

    @POST(RestMethodNames.INVITATIONS_CANCEL)
    fun cancelInvitation(
            @Path(RestMethodParams.INVITATION_ID) invitationGuid: String
    ): Completable

    @POST(RestMethodNames.INVITATIONS_INVITE)
    fun sendInvitation(
            @Path(RestMethodParams.USER_ID) userGuid: String
    ): Completable

    @POST(RestMethodNames.INVITATIONS_REJECT)
    fun rejectInvitation(
            @Path(RestMethodParams.INVITATION_ID) invitationGuid: String
    ): Completable

    @GET(RestMethodNames.INVITATIONS_RECEIVED)
    fun getInvitationsReceived(): Single<MutableList<Invitation>>

    @GET(RestMethodNames.INVITATIONS_SENT)
    fun getInvitationsSent(): Single<MutableList<Invitation>>

//Report
    @POST(RestMethodNames.REPORT_OTHER_USER)
    fun reportUser(
            @Path(RestMethodParams.USER_ID) userGuid: String,
            @Body reason: String
    ): Completable

//User
    @GET(RestMethodNames.USER_PROFILE)
    fun getUserProfile(): Single<AppUser>

    @GET(RestMethodNames.USER_PROFILE_OTHER)
    fun getUserProfile(
            @Path(RestMethodParams.USER_ID) userGuid: String
    ): Single<AppUser>

    @POST(RestMethodNames.USER_PROFILE_UPDATE)
    fun updateUserProfile(
            @Body appUser: AppUser
    ): Single<AppUser>

}

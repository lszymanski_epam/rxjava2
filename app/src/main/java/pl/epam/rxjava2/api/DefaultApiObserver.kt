package pl.epam.rxjava2.api

import android.support.annotation.CallSuper
import io.reactivex.CompletableObserver
import io.reactivex.MaybeObserver
import io.reactivex.SingleObserver
import io.reactivex.observers.DisposableObserver
import pl.epam.rxjava2.R
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import retrofit2.HttpException
import java.lang.ref.WeakReference


/**
 * Created by Lukasz Szymanski.
 */

open class DefaultApiObserver<T> (
        baseActivity: BaseActivity
) : DisposableObserver<T>(), CompletableObserver, MaybeObserver<T>, SingleObserver<T> {

    protected val weakRefBaseActivity: WeakReference<BaseActivity> = WeakReference(baseActivity)

    override fun onStart() {
        weakRefBaseActivity.get()?.hideKeyboard()
        weakRefBaseActivity.get()?.showLoadingIndicator(true)
    }

    override fun onNext(result: T) {

    }

    final override fun onSuccess(t: T) {
        if (t != Unit) {
            onNext(t)
        }

        onComplete()
    }

    @CallSuper
    override fun onError(e: Throwable) {
        if (weakRefBaseActivity.get() == null) {
            return
        }

        val baseActivity = weakRefBaseActivity.get()!!
        if (e is HttpException) {
            val httpException = e as HttpException
            baseActivity.showServerErrorMessage(e.response().errorBody(), e.response().code())
        } else {
            baseActivity.showErrorMessage(R.string.dialog_string_connection_problem_occurred)
        }

        baseActivity.showLoadingIndicator(false)
    }

    @CallSuper
    override fun onComplete() {
        weakRefBaseActivity.get()?.showLoadingIndicator(false)
    }
}
package pl.epam.rxjava2.api

import android.support.annotation.CallSuper
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.SwipeRefreshableView
import java.lang.ref.WeakReference

/**
 * Created by Lukasz Szymanski.
 */

open class SwipeRefreshableApiObserver<T> (
        baseActivity: BaseActivity,
        view: SwipeRefreshableView
) : DefaultApiObserver<T>(baseActivity) {

    protected val weakRefRefreshableView: WeakReference<SwipeRefreshableView> = WeakReference(view)

    override fun onStart() {
        weakRefBaseActivity.get()?.hideKeyboard()
        weakRefRefreshableView.get()?.showSwipeRefreshIndicator(true)
    }

    @CallSuper
    override fun onError(e: Throwable) {
        super.onError(e)
        weakRefRefreshableView.get()?.showSwipeRefreshIndicator(false)
    }

    @CallSuper
    override fun onComplete() {
        super.onComplete()
        weakRefRefreshableView.get()?.showSwipeRefreshIndicator(false)
    }
}
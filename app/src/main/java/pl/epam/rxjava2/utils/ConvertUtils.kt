package pl.epam.rxjava2.utils

import android.content.Context
import pl.epam.rxjava2.R
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.models.EditableKeyValue
import pl.epam.rxjava2.models.Skill
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import java.util.*

/**
 * Created by Lukasz Szymanski.
 */
object ConvertUtils {

    fun appUserToKeyValues(context: Context, baseFragment: BaseFragment, appUser: AppUser): List<EditableKeyValue> {
        val keyValuePairs = ArrayList<EditableKeyValue>()
        keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_email), getValueOrEmpty(appUser.email)))
        keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_firstname), getValueOrEmpty(appUser.firstName)))
        keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_lastname), getValueOrEmpty(appUser.lastName)))
        keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_phone_number),getValueOrEmpty(appUser.phone)))
        keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_city), getValueOrEmpty(appUser.city)))
        keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_age), getValueOrEmpty(appUser.age.toString())))

        val mainSkill = getSkill(appUser.skills, true, 0)
        val skill2 = getSkill(appUser.skills, false, 0)
        val skill3 = getSkill(appUser.skills, false, 1)

        keyValuePairs.add(
                EditableKeyValue(context.getString(R.string.skill_main),
                        getSkillName(mainSkill, context.getString(R.string.skill_choose)),
                        EditableKeyValue.TYPE_CHOOSABLE,
                        baseFragment,
                        mainSkill))
        keyValuePairs.add(
                EditableKeyValue(context.getString(R.string.skill_2),
                        getSkillName(skill2, context.getString(R.string.skill_choose)),
                        EditableKeyValue.TYPE_CHOOSABLE,
                        baseFragment,
                        skill2))
        keyValuePairs.add(
                EditableKeyValue(context.getString(R.string.skill_3),
                        getSkillName(skill3, context.getString(R.string.skill_choose)),
                        EditableKeyValue.TYPE_CHOOSABLE,
                        baseFragment,
                        skill3))

        return keyValuePairs
    }

    fun appOtherUserToKeyValues(context: Context, appUser: AppUser): List<EditableKeyValue> {
        val keyValuePairs = ArrayList<EditableKeyValue>()
        if (getValueOrEmpty(appUser.email).isNotEmpty()) {
            keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_email),
                    getValueOrEmpty(appUser.email),
                    EditableKeyValue.TYPE_BLOCKED))
        }
        keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_firstname),
                getValueOrEmpty(appUser.firstName),
                EditableKeyValue.TYPE_BLOCKED))
        keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_lastname),
                getValueOrEmpty(appUser.lastName),
                EditableKeyValue.TYPE_BLOCKED))
        if (getValueOrEmpty(appUser.phone).isNotEmpty()) {
            keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_phone_number),
                    getValueOrEmpty(appUser.phone),
                    EditableKeyValue.TYPE_BLOCKED))
        }
        keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_city),
                getValueOrEmpty(appUser.city),
                EditableKeyValue.TYPE_BLOCKED))
        keyValuePairs.add(EditableKeyValue(context.getString(R.string.prompt_age),
                getValueOrEmpty(appUser.age.toString()),
                EditableKeyValue.TYPE_BLOCKED))

        val mainSkill = getSkill(appUser.skills, true, 0)
        val skill2 = getSkill(appUser.skills, false, 0)
        val skill3 = getSkill(appUser.skills, false, 1)

        keyValuePairs.add(
                EditableKeyValue(context.getString(R.string.skill_main),
                        getSkillName(mainSkill, context.getString(R.string.skil_unknown)),
                        EditableKeyValue.TYPE_BLOCKED))
        keyValuePairs.add(
                EditableKeyValue(context.getString(R.string.skill_2),
                        getSkillName(skill2, context.getString(R.string.skil_unknown)),
                        EditableKeyValue.TYPE_BLOCKED))
        keyValuePairs.add(
                EditableKeyValue(context.getString(R.string.skill_3),
                        getSkillName(skill3, context.getString(R.string.skil_unknown)),
                        EditableKeyValue.TYPE_BLOCKED))

        return keyValuePairs
    }

    fun keyValuesToAppUser(context: Context, keyValuePairs: List<EditableKeyValue>): AppUser {
        val appUser = AppUser(guid = "")

        for (keyValue in keyValuePairs) {
            val value = getValueOrEmpty(keyValue.value)
            when (keyValue.key) {
                context.getString(R.string.prompt_email) -> appUser.email = value
                context.getString(R.string.prompt_firstname) -> appUser.firstName = value
                context.getString(R.string.prompt_lastname) -> appUser.lastName = value
                context.getString(R.string.prompt_phone_number) -> appUser.phone = value
                context.getString(R.string.prompt_city) -> appUser.city = value
                context.getString(R.string.prompt_age) ->  {
                    try {
                        appUser.age = value.toInt()
                    } catch (e: NumberFormatException) {
                        appUser.age = 0
                    }
                }
                context.getString(R.string.skill_main) -> {
                    val skill = keyValue.tag as? Skill
                    if (skill != null) {
                        appUser.skills.add(0, skill.copy(isMain = true))
                    }
                }
                context.getString(R.string.skill_2) -> {
                    val skill = keyValue.tag as? Skill
                    if (skill != null) {
                        appUser.skills.add(skill)
                    }
                }
                context.getString(R.string.skill_3) -> {
                    val skill = keyValue.tag as? Skill
                    if (skill != null) {
                        appUser.skills.add(skill)
                    }
                }
            }
        }

        return appUser
    }

    private fun getValueOrEmpty(value: String?) : String {
        return value ?: ""
    }

    private fun getSkillName(skill: Skill?, defaultName: String) : String {
        return skill?.name ?: defaultName
    }

    private fun getSkill(skills: MutableList<Skill>, isMain: Boolean, indexInList: Int) : Skill? {
        return skills.filter { it.isMain == isMain }
                .getOrNull(indexInList)
    }
}
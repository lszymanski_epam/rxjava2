package pl.epam.rxjava2.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.view.inputmethod.InputMethodManager

/**
 * Created by Lukasz Szymanski.
 */

object AppUtils {

    fun getAppVersion(context: Context): String {
        var versionText = ""
        try {
            versionText = context.packageManager.getPackageInfo(context.packageName, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return versionText
    }

    fun hideKeyboard(activity: Activity) {
        try {
            val inputManager = activity.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(activity.window.decorView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}

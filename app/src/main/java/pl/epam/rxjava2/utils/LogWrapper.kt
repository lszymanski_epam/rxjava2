package pl.epam.rxjava2.utils

import android.util.Log

import pl.epam.rxjava2.BuildConfig

/**
 * Created by Lukasz Szymanski.
 */

object LogWrapper {

    fun logDebug(tag: String, message: String) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, message)
        }
    }

    fun logError(tag: String, message: String) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, message)
        }
    }
}

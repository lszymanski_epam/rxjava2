package pl.epam.rxjava2.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import pl.epam.rxjava2.R
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.base.BaseRecyclerAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BaseFragment

/**
 * Created by Lukasz Szymanski.
 */

class SearchResultsAdapter (
        private val baseActivity: BaseActivity,
        private val baseFragment: BaseFragment
) : BaseRecyclerAdapter<BaseUser, SearchResultsAdapter.ViewHolder>(baseActivity) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.i_search_result, parent, false))
    }

    override fun setupHolder(item: BaseUser, holder: ViewHolder) {
        holder.tvName.text = String.format("%s %s", item.firstName, item.lastName)
        holder.tvAge.text = if (item.age != null) item.age.toString() else ""
        holder.tvCity.text = if (item.city != null) item.city else ""

        holder.rlSearchResult.tag = item
        holder.rlSearchResult.setOnClickListener {
            baseActivity.sendClickEvent(baseFragment, holder.rlSearchResult)
        }

        holder.ivSendInvitation.tag = item.userGuid
        holder.ivSendInvitation.setOnClickListener {
            baseActivity.sendClickEvent(baseFragment, holder.ivSendInvitation)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rlSearchResult: RelativeLayout
        val tvName: TextView
        val tvAge: TextView
        val tvCity: TextView
        val ivSendInvitation: ImageView

        init {
            rlSearchResult = itemView.findViewById<RelativeLayout>(R.id.rl_search_result)
            tvName = itemView.findViewById<TextView>(R.id.tv_full_name)
            tvAge = itemView.findViewById<TextView>(R.id.tv_age)
            tvCity = itemView.findViewById<TextView>(R.id.tv_city)
            ivSendInvitation = itemView.findViewById<ImageView>(R.id.iv_send_invitation)
        }
    }
}
package pl.epam.rxjava2.ui.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentStatePagerAdapter
import pl.epam.rxjava2.models.Article
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.screens.article.ArticleFragment

/**
 * Created by Lukasz Szymanski.
 */

class ArticlesPagerAdapter(
        fragment: BaseFragment,
        private val articles: MutableList<Article>
) : FragmentStatePagerAdapter(fragment.childFragmentManager) {

    override fun getItem(position: Int): Fragment? {
        return ArticleFragment.newInstance(articles.get(position))
    }

    override fun getCount(): Int {
        return articles.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return articles.get(position).title
    }

}
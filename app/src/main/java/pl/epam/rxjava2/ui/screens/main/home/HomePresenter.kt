package pl.epam.rxjava2.ui.screens.main.home

import com.arellomobile.mvp.InjectViewState
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BasePresenter
import javax.inject.Inject

/**
 * Created by Lukasz Szymanski.
 */

@InjectViewState
class HomePresenter(
        component: ActivityComponent
) : BasePresenter<HomeView>() {

    @Inject
    protected lateinit var apiService: ApiService
    @Inject
    protected lateinit var baseActivity: BaseActivity

    init {
        component.inject(this)
    }

    companion object {

        private val LOG_TAG = HomePresenter::class.java.simpleName
    }
}

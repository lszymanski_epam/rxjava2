package pl.epam.rxjava2.ui.screens.main.profile.data

import io.reactivex.Single
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.models.Skill
import pl.epam.rxjava2.models.SkillCategory
import pl.epam.rxjava2.ui.base.BaseRepository
import java.util.Calendar
import java.util.concurrent.TimeUnit

/**
 * Created by Lukasz Szymanski.
 */

class SkillsRepository(
        private val apiService: ApiService
) : BaseRepository() {

    override val expirationTimeInMillis = TimeUnit.MINUTES.toMillis(60)

    private var cachedSkillsList: EntityState<MutableList<Skill>>? = null
    private var cachedSkillCategories: EntityState<MutableList<SkillCategory>>? = null

    fun getAllSkills(): Single<MutableList<Skill>> {
        if (cachedSkillsList != null && ifNotExpired(cachedSkillsList!!.timestamp)) {
            return Single.just(cachedSkillsList!!.value)
        }

        cachedSkillsList = null
        return apiService.getAllSkills()
    }

    fun getAllSkillCategories(): Single<MutableList<SkillCategory>> {
        if (cachedSkillCategories != null && ifNotExpired(cachedSkillCategories!!.timestamp)) {
            return Single.just(cachedSkillCategories!!.value)
        }

        cachedSkillCategories = null
        return apiService.getAllSkillCategories()
    }

    fun initCachedAllSkills(skillsList: MutableList<Skill>) {
        if (cachedSkillsList == null)
            cachedSkillsList = EntityState(skillsList, Calendar.getInstance().timeInMillis)
    }

    fun initCachedAllSkillCategories(skillCategories: MutableList<SkillCategory>) {
        if (cachedSkillCategories == null)
            cachedSkillCategories = EntityState(skillCategories, Calendar.getInstance().timeInMillis)
    }
}
package pl.epam.rxjava2.ui.screens.info.about

import com.arellomobile.mvp.InjectViewState
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.ui.base.mvp.BasePresenter

/**
 * Created by Lukasz Szymanski.
 */

@InjectViewState
class AboutPresenter(
        component: ActivityComponent
) : BasePresenter<AboutView>() {

    init {
        component.inject(this)
    }

    companion object {

        private val LOG_TAG = AboutPresenter::class.java.simpleName
    }
}

package pl.epam.rxjava2.ui.dialogs

/**
 * Created by Lukasz Szymanski.
 */

class DialogMessage(
        val title: String,
        val message: String,
        val isLeftButtonVisible: Boolean = false,
        val callback: DialogCallback? = null
)

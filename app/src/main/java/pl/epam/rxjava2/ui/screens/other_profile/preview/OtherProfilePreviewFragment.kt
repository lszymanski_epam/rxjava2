package pl.epam.rxjava2.ui.screens.other_profile.preview

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.f_other_profile.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.constants.BundleKeys
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.ui.adapters.EditableKeyValueAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.dialogs.DialogCallback
import pl.epam.rxjava2.ui.dialogs.DialogCallbackWithResult
import pl.epam.rxjava2.ui.dialogs.WriteReasonDialog
import pl.epam.rxjava2.utils.ConvertUtils

/**
 * Created by Lukasz Szymanski.
 */

class OtherProfilePreviewFragment : BaseFragment(), OtherProfilePreviewView {

    @InjectPresenter
    lateinit var presenter: OtherProfilePreviewPresenter

    @ProvidePresenter
    fun providePresenter() = OtherProfilePreviewPresenter(component!!)

    private var component: ActivityComponent? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        daggerInjection()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_other_profile, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupView()
    }

    override fun onStart() {
        super.onStart()
        presenter.subscribe()

        val userGuid: String = arguments.getString(BundleKeys.EXTRA_USER_GUID)
        presenter.getUserProfile(userGuid)
    }

    override fun onStop() {
        super.onStop()
        presenter.unSubscribe()
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.iv_report -> onReportBtnClicked()
            R.id.iv_block -> onBlockBtnClicked()
            else -> super.onClickEvent(view)
        }
    }

    override fun updateOtherUserProfile(appUser: AppUser) {
        val keyValuePairs = ConvertUtils.appOtherUserToKeyValues(baseActivity!!, appUser)

        lv_profile_form.adapter = EditableKeyValueAdapter(baseActivity!!, keyValuePairs)

        iv_report.visibility = if (keyValuePairs.isNotEmpty()) View.VISIBLE else View.GONE
        iv_block.visibility = if (keyValuePairs.isNotEmpty()) View.VISIBLE else View.GONE
    }

    private fun daggerInjection() {
        component = getComponent(ActivityComponent::class.java)
    }

    private fun setupView() {
        sv_profile_form.isFocusableInTouchMode = true
        sv_profile_form.descendantFocusability = ViewGroup.FOCUS_BEFORE_DESCENDANTS
        lv_profile_form.isExpanded = true

        iv_report.setOnClickListener {
            baseActivity?.sendClickEvent(this, iv_report)
        }
        iv_block.setOnClickListener {
            baseActivity?.sendClickEvent(this, iv_block)
        }
    }

    private fun onReportBtnClicked() {
        baseActivity?.showMessage(
                R.string.dialog_string_title_warning,
                R.string.dialog_string_report_user_confirmation,
                true,
                object: DialogCallback() {
                    override fun rightConfirmBtnOnClick() {
                        val dialog = WriteReasonDialog.newInstance(baseActivity!!.getString(R.string.reason))
                        dialog.callback = object: DialogCallbackWithResult<String>() {
                            override fun rightConfirmBtnOnClick(result: String) {
                                val userGuid: String = arguments.getString(BundleKeys.EXTRA_USER_GUID)
                                presenter.reportUser(userGuid, result)
                            }
                        }
                        dialog.show(fragmentManager, WriteReasonDialog::class.java.name)
                    }
                })
    }

    private fun onBlockBtnClicked() {
        baseActivity?.showMessage(
                R.string.dialog_string_title_warning,
                R.string.dialog_string_block_user_confirmation,
                true,
                object: DialogCallback() {
                    override fun rightConfirmBtnOnClick() {
                        val userGuid: String = arguments.getString(BundleKeys.EXTRA_USER_GUID)
                        presenter.blockUser(userGuid)
                    }
                })
    }

    companion object {

        fun newInstance(userGuid: String): OtherProfilePreviewFragment {
            val args = Bundle()
            args.putString(BundleKeys.EXTRA_USER_GUID, userGuid)
            val fragment = OtherProfilePreviewFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
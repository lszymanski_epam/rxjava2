package pl.epam.rxjava2.ui.screens.main.invite

import com.arellomobile.mvp.MvpView
import pl.epam.rxjava2.models.Invitation
import pl.epam.rxjava2.ui.base.mvp.SwipeRefreshableView

/**
 * Created by Lukasz Szymanski.
 */

interface InvitationsView : MvpView, SwipeRefreshableView {

    fun updateInvitationsList(invitations: MutableList<Invitation>)

}
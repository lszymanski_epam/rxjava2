package pl.epam.rxjava2.ui.base.mvp

import android.support.annotation.CallSuper
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Lukasz Szymanski.
 */

open class BasePresenter<View: MvpView>() : MvpPresenter<View>() {

    protected val compositeDisposable: CompositeDisposable = CompositeDisposable()

    @CallSuper
    open fun subscribe() {

    }

    @CallSuper
    open fun unSubscribe() {
        compositeDisposable.clear()
    }

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

}
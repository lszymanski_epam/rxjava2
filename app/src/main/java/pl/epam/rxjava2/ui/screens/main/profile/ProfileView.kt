package pl.epam.rxjava2.ui.screens.main.profile

import android.view.View
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.models.SkillCategory

/**
 * Created by Lukasz Szymanski.
 */

@StateStrategyType(OneExecutionStateStrategy::class)
interface ProfileView : MvpView {

    fun updateUserProfile(appUser: AppUser)

    fun updateAllSkillsTree(skillsTree: MutableList<SkillCategory>)

    fun openSkillsListDialog(resultContainerView: View)

}
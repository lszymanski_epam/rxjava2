package pl.epam.rxjava2.ui.screens.other_profile.preview

import com.arellomobile.mvp.MvpView
import pl.epam.rxjava2.models.AppUser

/**
 * Created by Lukasz Szymanski.
 */

interface OtherProfilePreviewView : MvpView {

    fun updateOtherUserProfile(appUser: AppUser)

}
package pl.epam.rxjava2.ui.dialogs

/**
 * Created by Lukasz Szymanski.
 */

open class DialogCallback {

    open fun rightConfirmBtnOnClick() {

    }

    open fun leftBtnOnClick() {

    }

}

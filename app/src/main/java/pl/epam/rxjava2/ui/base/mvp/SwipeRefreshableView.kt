package pl.epam.rxjava2.ui.base.mvp

/**
 * Created by Lukasz Szymanski.
 */

interface SwipeRefreshableView {

    fun showSwipeRefreshIndicator(isVisible: Boolean)

}
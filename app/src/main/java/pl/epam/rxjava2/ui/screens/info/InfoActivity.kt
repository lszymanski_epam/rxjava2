package pl.epam.rxjava2.ui.screens.info

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import pl.epam.rxjava2.R
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.HasComponent
import pl.epam.rxjava2.ui.screens.info.about.AboutFragment

/**
 * Created by Lukasz Szymanski.
 */

class InfoActivity : BaseActivity(), HasComponent<ActivityComponent> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_container)

        if (intent.extras != null) {
            val screenType = intent.extras.getInt(EXTRA_INFO_SCREEN_TYPE)
            when (screenType) {
                ABOUT -> openFragment(R.id.container, AboutFragment.newInstance(), false)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getComponent(): ActivityComponent? {
        return activityComponent
    }

    companion object {

        private val EXTRA_INFO_SCREEN_TYPE = "EXTRA_INFO_SCREEN_TYPE"

        const val ABOUT = 1

        fun openAboutScreen(context: Context) {
            context.startActivity(getStartIntent(context, ABOUT))
        }

        fun getStartIntent(context: Context, infoScreenType: Int): Intent {
            val intent = Intent(context, InfoActivity::class.java)
            intent.putExtra(EXTRA_INFO_SCREEN_TYPE, infoScreenType)
            return intent
        }
    }
}

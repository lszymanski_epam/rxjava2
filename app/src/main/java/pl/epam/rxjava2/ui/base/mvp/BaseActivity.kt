package pl.epam.rxjava2.ui.base.mvp

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import okhttp3.ResponseBody
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pl.epam.rxjava2.R
import pl.epam.rxjava2.api.response.ServerErrorResponse
import pl.epam.rxjava2.app.App
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.di.components.AppComponent
import pl.epam.rxjava2.di.components.DaggerActivityComponent
import pl.epam.rxjava2.di.modules.BaseActivityModule
import pl.epam.rxjava2.listeners.ClickEventListener
import pl.epam.rxjava2.ui.dialogs.DialogCallback
import pl.epam.rxjava2.ui.dialogs.DialogMessage
import pl.epam.rxjava2.ui.dialogs.WarningDialog
import pl.epam.rxjava2.utils.AppUtils
import java.util.concurrent.TimeUnit

/**
 * Created by Lukasz Szymanski.
 */

abstract class BaseActivity : MvpAppCompatActivity(), ClickEventListener {

    protected val LOG_TAG: String = javaClass.simpleName

    protected var appComponent: AppComponent? = null
        private set
    protected var activityComponent: ActivityComponent? = null
        private set

    private val clickEventSubject: PublishSubject<Pair<ClickEventListener, View>> = PublishSubject.create()
    private val clickEventCompositeDisposable: CompositeDisposable = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        daggerInjection()
        super.onCreate(savedInstanceState)

        setupActionBar()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        subscribeClickEvents()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        unSubscribeClickEvents()
        super.onStop()
    }

    /** Throttle back button click with other clicks */
    override fun onBackPressed() {
        val view = View(this)
        view.id = BaseActivity.BACK_BUTTON_PRESSED_VIEW_ID
        sendClickEvent(this, view)
    }

    override fun onClickEvent(view: View) {
        val id = view.id
        if (id == BaseActivity.BACK_BUTTON_PRESSED_VIEW_ID) {
            super.onBackPressed()
        }
    }

    @CallSuper
    protected open fun daggerInjection() {
        appComponent = (application as App).appComponent
        activityComponent = DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .baseActivityModule(BaseActivityModule(this))
                .build()
    }

    /**
     * Replaces current fragment in container
     *
     * @param containerId       the container view id to replace
     * @param fragment          the fragment to attach to R.id.container (replacing previous if any)
     * @param addToBackStack    call [FragmentTransaction.addToBackStack] if true
     */
    fun openFragment(containerId: Int, fragment: Fragment, addToBackStack: Boolean) {
        if (!isFinishing) {
            hideKeyboard()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(containerId, fragment, getFragmentTag(fragment.javaClass))
            if (addToBackStack)
                transaction.addToBackStack(getFragmentTag(fragment.javaClass))
            transaction.commitAllowingStateLoss()
        }
    }

    fun closeFragment(containerId: Int) {
        if (!isFinishing) {
            val fragment = supportFragmentManager.findFragmentById(containerId)
            if (fragment != null) {
                supportFragmentManager.beginTransaction()
                        .remove(fragment)
                        .commitAllowingStateLoss()
            }
        }
    }

    fun clearBackStack() {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun showErrorMessage(@StringRes message: Int, isLeftButtonVisible: Boolean = false, callback: DialogCallback? = null) {
        showMessage(R.string.dialog_string_title_error, message, isLeftButtonVisible, callback)
    }

    fun showMessage(title: String, message: String, isLeftButtonVisible: Boolean = false, callback: DialogCallback? = null) {
        postMessage(DialogMessage(
                title,
                message,
                isLeftButtonVisible,
                callback))
    }

    fun showMessage(@StringRes title: Int, @StringRes message: Int, isLeftButtonVisible: Boolean = false, callback: DialogCallback? = null) {
        postMessage(DialogMessage(
                getString(title),
                getString(message),
                isLeftButtonVisible,
                callback))
    }

    fun showServerErrorMessage(jsonErrorResponseBody: ResponseBody?, responseCode: Int, callback: DialogCallback? = null) {
        try {
            val errorMessage = Gson().fromJson(jsonErrorResponseBody!!.string(), ServerErrorResponse::class.java)
            postMessage(DialogMessage(
                    getString(R.string.dialog_string_title_error),
                    errorMessage.error!!,
                    false,
                    callback))

        } catch (e: Exception) {

            postMessage(DialogMessage(
                    getString(R.string.dialog_string_title_error),
                    String.format(getString(R.string.dialog_string_server_error_message), responseCode),
                    false,
                    callback))
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onShowMessage(event: DialogMessage) {
        val dialog = WarningDialog.newInstance(
                event.title,
                event.message,
                event.isLeftButtonVisible,
                false)
        dialog.callback = event.callback
        dialog.show(supportFragmentManager, WarningDialog::class.java.name)

        EventBus.getDefault().removeStickyEvent(event)
    }

    /**
     * Shows/hides loading indicator
     * Base implementation consider that container has id: R.id.progress_bar.
     *
     * @param isLoading show indicator if true
     */
    fun showLoadingIndicator(isLoading: Boolean) {
        val view: View = findViewById(R.id.progress_bar)
        view.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    fun hideKeyboard() {
        AppUtils.hideKeyboard(this)
    }

    protected fun getFragmentTag(fragment: Class<*>): String {
        return fragment.name
    }

    fun sendClickEvent(source: ClickEventListener, view: View) {
        clickEventSubject.onNext(Pair(source, view))
    }

    private fun subscribeClickEvents() {
        clickEventCompositeDisposable.add(clickEventSubject
                .throttleFirst(BaseActivity.TIME_BETWEEN_CLICK_EVENTS_IN_MILLIS, TimeUnit.MILLISECONDS)
                .subscribe { (source, view) ->
                    source.onClickEvent(view)
                })
    }

    private fun unSubscribeClickEvents() {
        clickEventCompositeDisposable.clear()
    }

    private fun postMessage(dialogMessage: DialogMessage) {
        EventBus.getDefault().postSticky(dialogMessage)
    }

    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    companion object {

        private val TIME_BETWEEN_CLICK_EVENTS_IN_MILLIS = 500L
        private val BACK_BUTTON_PRESSED_VIEW_ID = -1

    }
}

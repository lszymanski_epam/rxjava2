package pl.epam.rxjava2.ui.base

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pl.epam.rxjava2.ui.base.mvp.BaseActivity

/**
 * Created by Lukasz Szymanski.
 */

abstract class BaseRecyclerAdapter<T, VH : RecyclerView.ViewHolder>(
        protected val activity: BaseActivity
) : RecyclerView.Adapter<VH>() {

    protected val inflater: LayoutInflater = LayoutInflater.from(activity)
    protected var objects: MutableList<T> = mutableListOf()

    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH

    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = getItem(holder.adapterPosition)
        setupHolder(item, holder)
    }

    protected abstract fun setupHolder(item: T, holder: VH)

    open fun setItems(items: MutableList<T>) {
        objects = items
        notifyDataSetChanged()
    }

    open fun clear() {
        val size = objects.size
        objects.clear()
        notifyItemRangeRemoved(0, size)
    }

    open fun addItem(item: T) {
        objects.add(item)
        notifyItemInserted(itemCount - 1)
    }

    open fun addAllItems(items: MutableList<T>?) {
        if (items == null || items.size == 0)
            return

        objects.addAll(items)
        notifyItemRangeInserted(itemCount - items.size, items.size)
    }

    open fun removeItem(item: T) {
        val position = objects.indexOfFirst { it == item }
        if (position >= 0) {
            objects.remove(item)
            notifyItemRemoved(position)
        }
    }

    open fun getItem(position: Int): T {
        return objects[position]
    }

    operator fun contains(item: T): Boolean {
        return objects.contains(item)
    }

    override fun getItemCount(): Int {
        return objects.size
    }
}

package pl.epam.rxjava2.ui.screens.main.experts

import com.arellomobile.mvp.MvpView
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.base.mvp.SwipeRefreshableView

/**
 * Created by Lukasz Szymanski.
 */

interface ExpertsView : MvpView, SwipeRefreshableView {

    fun appendExpertsList(experts: MutableList<BaseUser>)

    fun removeExpertFromAdapter(userGuid: String)

}
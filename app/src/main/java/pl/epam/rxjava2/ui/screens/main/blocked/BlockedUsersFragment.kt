package pl.epam.rxjava2.ui.screens.main.blocked

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.f_blocked_users.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.adapters.BlockedUsersAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.dialogs.DialogCallback
import pl.epam.rxjava2.ui.screens.other_profile.OtherUserProfileActivity

/**
 * Created by Lukasz Szymanski.
 */

class BlockedUsersFragment : BaseFragment(), BlockedUsersView {

    @InjectPresenter
    lateinit var presenter: BlockedUsersPresenter

    @ProvidePresenter
    fun providePresenter() = BlockedUsersPresenter(component!!)

    private var component: ActivityComponent? = null
    private var blockedUsersAdapter: BlockedUsersAdapter? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        daggerInjection()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_blocked_users, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupView()
    }

    override fun onStart() {
        super.onStart()
        presenter.subscribe()

        presenter.getBlockedUsersList()
    }

    override fun onStop() {
        super.onStop()
        presenter.unSubscribe()
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.iv_remove -> {
                val expert : BaseUser? = view.tag as? BaseUser
                onRemoveBlockedUserButtonClick(expert)
            }
            R.id.rl_blocked_user -> {
                val baseUser: BaseUser? = view.tag as? BaseUser
                if (baseUser != null)
                    OtherUserProfileActivity.openActivity(baseActivity!!, baseUser.userGuid)
            }
            else -> super.onClickEvent(view)
        }
    }

    override fun showSwipeRefreshIndicator(isVisible: Boolean) {
        swipe_refresh_layout.isRefreshing = isVisible
    }

    override fun updateBlockedUsersList(blockedUsers: MutableList<BaseUser>) {
        blockedUsersAdapter?.setItems(blockedUsers)
    }

    private fun daggerInjection() {
        component = getComponent(ActivityComponent::class.java)
    }

    private fun setupView() {
        setupAdapter()
        setupRecyclerView()
        setupRefreshLayout()
    }

    private fun setupAdapter() {
        blockedUsersAdapter = BlockedUsersAdapter(baseActivity!!, this)
    }

    private fun setupRecyclerView() {
        rv_blocked_users.adapter = blockedUsersAdapter
        rv_blocked_users.layoutManager = LinearLayoutManager(baseActivity)
        rv_blocked_users.addItemDecoration(DividerItemDecoration(baseActivity, DividerItemDecoration.VERTICAL))
    }

    private fun setupRefreshLayout() {
        swipe_refresh_layout.setOnRefreshListener {
            presenter.getBlockedUsersList()
        }
    }

    private fun onRemoveBlockedUserButtonClick(expert: BaseUser?) {
        if (expert != null) {
            baseActivity?.showMessage(
                    R.string.dialog_string_title_warning,
                    R.string.dialog_string_unblock_user_confirmation,
                    true,
                    object: DialogCallback() {
                        override fun rightConfirmBtnOnClick() {
                            presenter.unblockUser(expert.userGuid)
                        }
                    })
        }
    }

    companion object {

        fun newInstance(): BlockedUsersFragment {
            return BlockedUsersFragment()
        }
    }
}
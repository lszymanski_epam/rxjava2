package pl.epam.rxjava2.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import pl.epam.rxjava2.R
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.base.BaseRecyclerAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BaseFragment

/**
 * Created by Lukasz Szymanski.
 */
class BlockedUsersAdapter(
        private val baseActivity: BaseActivity,
        private val baseFragment: BaseFragment
) : BaseRecyclerAdapter<BaseUser, BlockedUsersAdapter.ViewHolder>(baseActivity) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.i_blocked_user, parent, false))
    }

    override fun setupHolder(item: BaseUser, holder: ViewHolder) {
        holder.tvName.text = String.format("%s %s", item.firstName, item.lastName)
        holder.tvAge.text = if (item.age != null) item.age.toString() else ""
        holder.tvCity.text = if (item.city != null) item.city else ""

        holder.rlBlockedUser.tag = item
        holder.rlBlockedUser.setOnClickListener {
            baseActivity.sendClickEvent(baseFragment, holder.rlBlockedUser)
        }

        holder.ivRemove.tag = item
        holder.ivRemove.setOnClickListener {
            baseActivity.sendClickEvent(baseFragment, holder.ivRemove)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rlBlockedUser: RelativeLayout
        val tvName: TextView
        val tvAge: TextView
        val tvCity: TextView
        val ivRemove: ImageView

        init {
            rlBlockedUser = itemView.findViewById<RelativeLayout>(R.id.rl_blocked_user)
            tvName = itemView.findViewById<TextView>(R.id.tv_full_name)
            tvAge = itemView.findViewById<TextView>(R.id.tv_age)
            tvCity = itemView.findViewById<TextView>(R.id.tv_city)
            ivRemove = itemView.findViewById<ImageView>(R.id.iv_remove)
        }
    }
}
package pl.epam.rxjava2.ui.screens.main.profile.data

/**
 * Created by Lukasz Szymanski on 08.04.2018.
 */

data class EntityState<T>(
    var value: T,
    var timestamp: Long
)
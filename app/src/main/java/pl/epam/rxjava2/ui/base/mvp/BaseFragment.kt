package pl.epam.rxjava2.ui.base.mvp

import android.content.Context
import android.view.View
import com.arellomobile.mvp.MvpAppCompatFragment
import pl.epam.rxjava2.listeners.ClickEventListener

/**
 * Created by Lukasz Szymanski.
 */

abstract class BaseFragment : MvpAppCompatFragment(), ClickEventListener {

    protected val LOG_TAG: String = javaClass.simpleName

    protected var baseActivity: BaseActivity? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            baseActivity = context as BaseActivity?
        } catch (ex: ClassCastException) {
            throw ClassCastException(activity.javaClass.simpleName + " must be of class " + BaseActivity::class.java.simpleName)
        }
    }

    override fun onClickEvent(view: View) {
        // no-op
    }

    /**
     * Gets a component for dependency injection by its type.
     */
    @Suppress("UNCHECKED_CAST")
    protected fun <C> getComponent(componentType: Class<C>): C {
        return componentType.cast((baseActivity as HasComponent<C>).getComponent())
    }

}

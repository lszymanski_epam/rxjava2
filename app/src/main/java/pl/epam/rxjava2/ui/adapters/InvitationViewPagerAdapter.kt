package pl.epam.rxjava2.ui.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import pl.epam.rxjava2.R
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.screens.main.invite.received.InvitationsReceivedFragment
import pl.epam.rxjava2.ui.screens.main.invite.sent.InvitationsSentFragment

/**
 * Created by Lukasz Szymanski.
 */

class InvitationViewPagerAdapter(
        private val fragment: BaseFragment
) : FragmentPagerAdapter(fragment.childFragmentManager) {

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return InvitationsReceivedFragment.newInstance()
            1 -> return InvitationsSentFragment.newInstance()
        }
        return null
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        if (fragment.activity != null) {
            when (position) {
                0 -> return fragment.activity.getString(R.string.received)
                1 -> return fragment.activity.getString(R.string.sent)
            }
        }
        return ""
    }

}
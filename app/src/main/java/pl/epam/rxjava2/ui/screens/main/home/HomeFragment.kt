package pl.epam.rxjava2.ui.screens.main.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.f_home.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.screens.search_results.SearchResultsActivity
import java.util.Locale

class HomeFragment : BaseFragment(), HomeView {

    @InjectPresenter
    lateinit var presenter: HomePresenter

    @ProvidePresenter
    fun providePresenter() = HomePresenter(component!!)

    private var component: ActivityComponent? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        daggerInjection()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_home, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupView()
    }

    override fun onStart() {
        super.onStart()
        presenter.subscribe()
    }

    override fun onStop() {
        super.onStop()
        presenter.unSubscribe()
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.fab -> {
                val minAge = seekbar_expert_age.selectedMinValue.toInt()
                val maxAge = seekbar_expert_age.selectedMaxValue.toInt()
                SearchResultsActivity.openActivity(baseActivity!!, minAge, maxAge)
            }
            else -> super.onClickEvent(view)
        }
    }

    private fun daggerInjection() {
        component = getComponent(ActivityComponent::class.java)
    }

    private fun setupView() {
        seekbar_expert_age.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            if (maxValue.toInt() < 60)
                tv_expert_age_value.text = String.format(Locale.US, "%d - %d", minValue.toInt(), maxValue.toInt())
            else
                tv_expert_age_value.text = String.format(Locale.US, "%d - %d+", minValue.toInt(), maxValue.toInt())
        }

        fab.setOnClickListener {
            baseActivity?.sendClickEvent(this, fab)
        }
    }

    companion object {

        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }
}

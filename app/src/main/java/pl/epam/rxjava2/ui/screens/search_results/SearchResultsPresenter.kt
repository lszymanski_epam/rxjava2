package pl.epam.rxjava2.ui.screens.search_results

import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.epam.rxjava2.R
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.api.DefaultApiObserver
import pl.epam.rxjava2.api.SwipeRefreshableApiObserver
import pl.epam.rxjava2.app.Settings
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.models.SearchExpertsParams
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BasePresenter
import javax.inject.Inject

/**
 * Created by Lukasz Szymanski.
 */

@InjectViewState
class SearchResultsPresenter(
        component: ActivityComponent
) : BasePresenter<SearchResultsView>() {

    @Inject
    protected lateinit var apiService: ApiService
    @Inject
    protected lateinit var baseActivity: BaseActivity

    init {
        component.inject(this)
    }

    /**
     * If lastUserGuid == null, than we download first Settings.DEFAULT_AMOUNT_OF_USERS users
     */
    fun searchExperts(minAge: Int, maxAge: Int, lastUserGuid: String?) {
        compositeDisposable.add(
                apiService.searchExperts(minAge,
                        maxAge,
                        Settings.DEFAULT_AMOUNT_OF_USERS,
                        lastUserGuid ?: Settings.EMPTY_GUID)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object: SwipeRefreshableApiObserver<MutableList<BaseUser>>(baseActivity, viewState) {
                        override fun onStart() {
                            if (lastUserGuid != null) {
                                super.onStart()
                            } else {
                                baseActivity.showLoadingIndicator(true)
                            }
                        }

                        override fun onNext(result: MutableList<BaseUser>) {
                            viewState.appendSearchResults(result)
                        }
                    }))
    }

    fun sendInvitation(userGuid: String, searchExpertsParams: SearchExpertsParams) {
        compositeDisposable.add(apiService.sendInvitation(userGuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<Unit>(baseActivity) {
                    override fun onComplete() {
                        super.onComplete()

                        baseActivity.showMessage(
                                R.string.dialog_string_title_success,
                                R.string.dialog_string_invitation_sent)
                    }
                }))
    }

    companion object {

        private val LOG_TAG = SearchResultsPresenter::class.java.simpleName
    }
}
package pl.epam.rxjava2.ui.base.mvp

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment
import android.view.View
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import pl.epam.rxjava2.listeners.ClickEventListener
import java.util.concurrent.TimeUnit

/**
 * Created by Lukasz Szymanski.
 */

open class BaseDialogFragment : MvpAppCompatDialogFragment(), ClickEventListener {

    protected val LOG_TAG: String = javaClass.simpleName

    private val clickEventSubject: PublishSubject<Pair<ClickEventListener, View>> = PublishSubject.create()
    private val clickEventCompositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, 0);
    }

    override fun onStart() {
        super.onStart()
        subscribeClickEvents()
    }

    override fun onStop() {
        unSubscribeClickEvents()
        super.onStop()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onClickEvent(view: View) {
        // no-op
    }

    /**
     * Gets a component for dependency injection by its type.
     */
    @Suppress("UNCHECKED_CAST")
    protected fun <C> getComponent(componentType: Class<C>): C? {
        return componentType.cast((activity as HasComponent<C>).getComponent())
    }

    fun sendClickEvent(source: ClickEventListener, view: View) {
        clickEventSubject.onNext(Pair(source, view))
    }

    private fun subscribeClickEvents() {
        clickEventCompositeDisposable.add(clickEventSubject
                .throttleFirst(BaseDialogFragment.TIME_BETWEEN_CLICK_EVENTS_IN_MILLIS, TimeUnit.MILLISECONDS)
                .subscribe { (source, view) ->
                    source.onClickEvent(view)
                })
    }

    private fun unSubscribeClickEvents() {
        clickEventCompositeDisposable.clear()
    }

    companion object {

        private val TIME_BETWEEN_CLICK_EVENTS_IN_MILLIS = 500L

    }

}

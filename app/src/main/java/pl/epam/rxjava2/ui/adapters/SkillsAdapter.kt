package pl.epam.rxjava2.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation.RELATIVE_TO_SELF
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import pl.epam.rxjava2.R
import pl.epam.rxjava2.listeners.ClickEventListener
import pl.epam.rxjava2.models.SkillCategory
import pl.epam.rxjava2.ui.base.mvp.BaseActivity


/**
 * Created by Lukasz Szymanski.
 */

class SkillsAdapter(
        groups: List<SkillCategory>,
        private val baseActivity: BaseActivity,
        private val clickEventListener: ClickEventListener
) : ExpandableRecyclerViewAdapter<SkillsAdapter.SkillCategoryViewHolder, SkillsAdapter.SkillsViewHolder>(groups) {

    private val inflater: LayoutInflater = LayoutInflater.from(baseActivity)

    override fun onCreateGroupViewHolder(parent: ViewGroup, viewType: Int): SkillCategoryViewHolder {
        val view = inflater.inflate(R.layout.i_skill_category, parent, false)
        return SkillCategoryViewHolder(view)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup, viewType: Int): SkillsViewHolder {
        val view = inflater.inflate(R.layout.i_skill, parent, false)
        return SkillsViewHolder(view)
    }

    override fun onBindGroupViewHolder(holder: SkillCategoryViewHolder, flatPosition: Int, group: ExpandableGroup<*>) {
        holder.tvCategoryName.text = group.title
    }

    override fun onBindChildViewHolder(holder: SkillsViewHolder, flatPosition: Int, group: ExpandableGroup<*>, childIndex: Int) {
        val skill = (group as SkillCategory).items[childIndex]
        holder.skillName.text = skill.name
        holder.skillName.tag = skill
        holder.skillName.setOnClickListener {
            baseActivity.sendClickEvent(clickEventListener, holder.skillName)
        }
    }

    //---------------------------------------------------------------------------

    class SkillCategoryViewHolder(itemView: View): GroupViewHolder(itemView) {

        val tvCategoryName: TextView = itemView.findViewById<TextView>(R.id.tv_category_name)
        val ivArrow: ImageView = itemView.findViewById<ImageView>(R.id.iv_arrow)

        override fun expand() {
            val rotate = RotateAnimation(360f, 180f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f)
            rotate.duration = 300
            rotate.fillAfter = true
            ivArrow.animation = rotate
        }

        override fun collapse() {
            val rotate = RotateAnimation(180f, 360f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f)
            rotate.duration = 300
            rotate.fillAfter = true
            ivArrow.animation = rotate
        }
    }


    class SkillsViewHolder(itemView: View): ChildViewHolder(itemView) {

        val skillName: TextView = itemView.findViewById<TextView>(R.id.tv_skill_name)

    }
}
package pl.epam.rxjava2.ui.screens.main.profile

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.f_profile.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.models.EditableKeyValue
import pl.epam.rxjava2.models.Skill
import pl.epam.rxjava2.models.SkillCategory
import pl.epam.rxjava2.ui.adapters.EditableKeyValueAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.dialogs.DialogCallbackWithResult
import pl.epam.rxjava2.ui.dialogs.SkillsListDialog
import pl.epam.rxjava2.utils.ConvertUtils

/**
 * Created by Lukasz Szymanski.
 */
class ProfileFragment : BaseFragment(), ProfileView {

    @InjectPresenter
    lateinit var presenter: ProfilePresenter

    @ProvidePresenter
    fun providePresenter() = ProfilePresenter(component!!)

    private var component: ActivityComponent? = null
    private var allSkillsTree: MutableList<SkillCategory>? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        daggerInjection()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_profile, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupView()
        presenter.downloadAllSkills()
    }

    override fun onStart() {
        super.onStart()
        presenter.subscribe()

        presenter.getUserProfile()
    }

    override fun onStop() {
        super.onStop()
        presenter.unSubscribe()
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.btn_save_changes -> {
                val keyValuePairs = (lv_profile_form.adapter as EditableKeyValueAdapter).items
                presenter.updateUserProfile(ConvertUtils.keyValuesToAppUser(baseActivity!!, keyValuePairs))
            }
            R.id.et_value -> {
               presenter.openSkillsListDialog(view)
            }
            else -> super.onClickEvent(view)
        }
    }

    override fun updateUserProfile(appUser: AppUser) {
        val keyValuePairs = ConvertUtils.appUserToKeyValues(baseActivity!!, this, appUser)

        lv_profile_form.adapter = EditableKeyValueAdapter(baseActivity!!, keyValuePairs)

        btn_save_changes.visibility = if (keyValuePairs.isNotEmpty()) View.VISIBLE else View.GONE
    }

    override fun updateAllSkillsTree(skillsTree: MutableList<SkillCategory>) {
        allSkillsTree = skillsTree
    }

    override fun openSkillsListDialog(resultContainerView: View) {
        if (allSkillsTree != null) {
            val dialog = SkillsListDialog.newInstance(
                    getString(R.string.skills),
                    allSkillsTree!!)

            dialog.callback = object : DialogCallbackWithResult<Any>() {
                override fun rightConfirmBtnOnClick(result: Any) {
                    if (resultContainerView is EditText && result is Skill) {
                        if (resultContainerView.tag is EditableKeyValue) {
                            val editableKeyValueTag = resultContainerView.tag as EditableKeyValue
                            editableKeyValueTag.tag = result
                            resultContainerView.setText(result.name)
                        }
                    }
                }
            }

            dialog.show(fragmentManager, SkillsListDialog::class.java.name)
        }
    }

    private fun daggerInjection() {
        component = getComponent(ActivityComponent::class.java)
    }

    private fun setupView() {
        sv_profile_form.isFocusableInTouchMode = true
        sv_profile_form.descendantFocusability = ViewGroup.FOCUS_BEFORE_DESCENDANTS
        lv_profile_form.isExpanded = true

        btn_save_changes.setOnClickListener {
            baseActivity?.sendClickEvent(this, btn_save_changes)
        }
    }

    companion object {

        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }
}

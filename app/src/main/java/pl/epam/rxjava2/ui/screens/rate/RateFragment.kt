package pl.epam.rxjava2.ui.screens.rate

import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.jakewharton.rxbinding2.widget.RxCompoundButton
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.rxkotlin.Observables
import kotlinx.android.synthetic.main.f_rate.*
import kotlinx.android.synthetic.main.incl_articles.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.constants.BundleKeys
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.Article
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.adapters.ArticlesPagerAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseFragment

/**
 * Created by Lukasz Szymanski.
 */

class RateFragment : BaseFragment(), RateView {

    @InjectPresenter
    lateinit var presenter: RatePresenter

    @ProvidePresenter
    fun providePresenter() = RatePresenter(component!!)

    private var component: ActivityComponent? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        daggerInjection()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_rate, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupView()
    }

    override fun onStart() {
        super.onStart()
        presenter.subscribe()

        presenter.addDisposable(Observables.combineLatest(RxTextView.textChangeEvents(et_opinion),
                RxCompoundButton.checkedChanges(cb_agreement),
                { opinionTextObservable, agreementObservable ->
                    val opinionTextCheck = opinionTextObservable.text().length >= 3

                    opinionTextCheck && agreementObservable
                })
                .subscribe({ aBoolean ->
                    val backgroundRes = if (aBoolean) R.drawable.ripple else R.drawable.bg_rounded_button_disabled

                    btn_submit_rate.isEnabled = aBoolean
                    btn_submit_rate.background = ContextCompat.getDrawable(context, backgroundRes)
                }))
    }

    override fun onStop() {
        super.onStop()
        presenter.unSubscribe()
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.iv_cancel -> {
                updateArticles(mutableListOf())
                presenter.downloadArticles()
            }
            R.id.btn_submit_rate -> {
                val baseUser = arguments.getParcelable<BaseUser>(BundleKeys.EXTRA_BASE_USER)
                presenter.rateExpert(baseUser.userGuid, cb_agreement.isChecked, et_opinion.text.toString())
                et_opinion.setText("")
            }
            else -> super.onClickEvent(view)
        }
    }

    override fun updateArticles(articles: MutableList<Article>) {
        vp_articles.adapter = ArticlesPagerAdapter(this, articles)

        layout_articles.visibility = if (articles.isNotEmpty()) View.VISIBLE else View.GONE
    }

    override fun rotateArticles() {
        vp_articles.setCurrentItem((vp_articles.currentItem + 1) % vp_articles.adapter.count, true)
    }

    private fun daggerInjection() {
        component = getComponent(ActivityComponent::class.java)
    }

    private fun setupView() {
        setupActionBarView()
        setupButtons()
    }

    private fun setupActionBarView() {
        val baseUser = arguments.getParcelable<BaseUser>(BundleKeys.EXTRA_BASE_USER)
        val userName = "${baseUser.firstName} ${baseUser.lastName}"
        baseActivity?.supportActionBar?.setTitle(userName)
    }

    private fun setupButtons() {
        iv_cancel.setOnClickListener {
            baseActivity?.sendClickEvent(this, iv_cancel)
        }

        btn_submit_rate.setOnClickListener {
            baseActivity?.sendClickEvent(this, btn_submit_rate)
        }
    }

    companion object {

        fun newInstance(baseUser: BaseUser): RateFragment {
            val args = Bundle()
            args.putParcelable(BundleKeys.EXTRA_BASE_USER, baseUser)
            val fragment = RateFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
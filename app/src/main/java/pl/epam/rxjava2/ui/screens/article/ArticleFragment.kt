package pl.epam.rxjava2.ui.screens.article

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.f_article.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.constants.BundleKeys
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.Article
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.screens.web_page.WebPageActivity

/**
 * Created by Lukasz Szymanski.
 */

class ArticleFragment : BaseFragment(), ArticleView {

    @InjectPresenter
    lateinit var presenter: ArticlePresenter

    @ProvidePresenter
    fun providePresenter() = ArticlePresenter(component!!)

    private var component: ActivityComponent? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        daggerInjection()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_article, container, false)
    }

    override fun onStart() {
        super.onStart()
        presenter.subscribe()

        setupView()
    }

    override fun onStop() {
        super.onStop()
        presenter.unSubscribe()
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.ll_article -> {
                val article = ll_article.tag as? Article
                if (article != null) {
                    WebPageActivity.openActivity(baseActivity!!,
                            article.website, article.title, null)
                }
            }
            else -> super.onClickEvent(view)
        }
    }

    private fun daggerInjection() {
        component = getComponent(ActivityComponent::class.java)
    }

    private fun setupView() {
        val article = arguments.getParcelable<Article>(BundleKeys.EXTRA_ARTICLE)

        Picasso.with(baseActivity).load(article.imageUrl).into(iv_article_image)
        tv_article_title.text = article.title
        tv_article_source.text = article.source

        ll_article.tag = article
        ll_article.setOnClickListener {
            baseActivity?.sendClickEvent(this, ll_article)
        }
    }

    companion object {

        fun newInstance(article: Article): ArticleFragment {
            val args = Bundle()
            args.putParcelable(BundleKeys.EXTRA_ARTICLE, article)
            val fragment = ArticleFragment()
            fragment.arguments = args
            return fragment
        }
    }
}

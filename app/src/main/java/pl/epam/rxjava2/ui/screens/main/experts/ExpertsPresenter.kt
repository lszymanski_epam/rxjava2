package pl.epam.rxjava2.ui.screens.main.experts

import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import pl.epam.rxjava2.R
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.api.DefaultApiObserver
import pl.epam.rxjava2.app.Settings
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BasePresenter
import javax.inject.Inject

/**
 * Created by Lukasz Szymanski.
 */

@InjectViewState
class ExpertsPresenter(
        component: ActivityComponent
) : BasePresenter<ExpertsView>() {

    @Inject
    protected lateinit var apiService: ApiService
    @Inject
    protected lateinit var baseActivity: BaseActivity

    private val pagination = PublishProcessor.create<String>()

    init {
        component.inject(this)
    }

    override fun subscribe() {
        super.subscribe()
        setupExpertsListUpdates()
    }

    /**
     * If lastUserGuid == null, than we download first Settings.DEFAULT_AMOUNT_OF_USERS users
     */
    fun getExpertsList(lastExpertGuid: String?) {
        if (lastExpertGuid != null) {
            viewState.showSwipeRefreshIndicator(true)
        } else {
            baseActivity.showLoadingIndicator(true)
        }
        pagination.onNext(lastExpertGuid ?: Settings.EMPTY_GUID)
    }

    fun removeExpert(userGuid: String) {
        compositeDisposable.add(apiService.removeMyExpert(userGuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<MutableList<BaseUser>>(baseActivity) {
                    override fun onComplete() {
                        super.onComplete()
                        viewState.removeExpertFromAdapter(userGuid)

                        baseActivity.showMessage(
                                R.string.dialog_string_title_success,
                                R.string.dialog_string_expert_removed)
                    }
                }))

    }

    private fun setupExpertsListUpdates() {
        compositeDisposable.add(pagination
                .onBackpressureBuffer()
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.io())
                .concatMap {lastExpertGuid ->
                    apiService.getMyExpertsList(
                            Settings.DEFAULT_AMOUNT_OF_USERS,
                            lastExpertGuid)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext{result ->
                    viewState.appendExpertsList(result)
                    viewState.showSwipeRefreshIndicator(false)
                    baseActivity.showLoadingIndicator(false)
                }
                .doOnError{
                    baseActivity.showErrorMessage(R.string.dialog_string_connection_problem_occurred)
                    viewState.showSwipeRefreshIndicator(false)
                    baseActivity.showLoadingIndicator(false)
                }
                .subscribe())
    }

    companion object {

        private val LOG_TAG = ExpertsPresenter::class.java.simpleName
    }
}
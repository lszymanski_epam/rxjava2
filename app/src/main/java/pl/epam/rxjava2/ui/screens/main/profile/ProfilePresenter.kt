package pl.epam.rxjava2.ui.screens.main.profile

import android.view.View
import com.arellomobile.mvp.InjectViewState
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import pl.epam.rxjava2.R
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.api.DefaultApiObserver
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.models.Skill
import pl.epam.rxjava2.models.SkillCategory
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BasePresenter
import pl.epam.rxjava2.ui.screens.main.profile.data.ProfileRepository
import pl.epam.rxjava2.ui.screens.main.profile.data.SkillsRepository
import javax.inject.Inject

/**
 * Created by Lukasz Szymanski.
 */

@InjectViewState
class ProfilePresenter(
        component: ActivityComponent
) : BasePresenter<ProfileView>() {

    @Inject
    protected lateinit var apiService: ApiService
    @Inject
    protected lateinit var baseActivity: BaseActivity
    @Inject
    protected lateinit var profileRepository: ProfileRepository
    @Inject
    protected lateinit var skillsRepository: SkillsRepository

    init {
        component.inject(this)
    }

    fun getUserProfile() {
        compositeDisposable.add(profileRepository.getUserProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<AppUser>(baseActivity) {
                    override fun onNext(result: AppUser) {
                        profileRepository.initCachedUserProfile(result, false)
                        viewState.updateUserProfile(result)
                    }
                }))
    }

    fun updateUserProfile(appUser: AppUser) {
        compositeDisposable.add(apiService.updateUserProfile(appUser)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<AppUser>(baseActivity) {
                    override fun onNext(result: AppUser) {
                        profileRepository.initCachedUserProfile(result, true)
                        viewState.updateUserProfile(result)
                    }

                    override fun onComplete() {
                        super.onComplete()

                        baseActivity.showMessage(
                                R.string.dialog_string_title_success,
                                R.string.dialog_string_profile_updated
                        )
                    }
                }))
    }

    fun downloadAllSkills() {
        compositeDisposable.add(
                Single.zip(skillsRepository.getAllSkills(),
                        skillsRepository.getAllSkillCategories(),
                        object : BiFunction<MutableList<Skill>, MutableList<SkillCategory>, MutableList<SkillCategory>> {
                            @Throws(Exception::class)
                            override fun apply(skills: MutableList<Skill>, skillCategories: MutableList<SkillCategory>): MutableList<SkillCategory> {
                                skillsRepository.initCachedAllSkills(skills)
                                skillsRepository.initCachedAllSkillCategories(skillCategories)

                                val newList: MutableList<SkillCategory> = mutableListOf()
                                skillCategories.forEach {
                                    val categoryId = it.id
                                    newList.add(SkillCategory(categoryId,
                                            it.name,
                                            skills.filter { it.categoryId == categoryId }.toMutableList()))
                                }

                                return newList
                            }
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object: DefaultApiObserver<MutableList<SkillCategory>>(baseActivity) {
                            override fun onNext(result: MutableList<SkillCategory>) {
                                viewState.updateAllSkillsTree(result)
                            }
                        })
        )
    }

    fun openSkillsListDialog(resultContainerView: View) {
        viewState.openSkillsListDialog(resultContainerView)
    }

    companion object {

        private val LOG_TAG = ProfilePresenter::class.java.simpleName
    }
}
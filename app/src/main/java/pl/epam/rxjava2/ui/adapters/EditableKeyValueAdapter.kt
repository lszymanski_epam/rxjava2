package pl.epam.rxjava2.ui.adapters

import android.content.Context
import android.support.design.widget.TextInputLayout
import android.text.InputType
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import pl.epam.rxjava2.R
import pl.epam.rxjava2.listeners.KeyValueTextWatcher
import pl.epam.rxjava2.models.EditableKeyValue
import pl.epam.rxjava2.ui.base.mvp.BaseActivity

/**
 * Created by Lukasz Szymanski.
 */

class EditableKeyValueAdapter(
        private val baseActivity: BaseActivity,
        val items: List<EditableKeyValue>
) : BaseAdapter() {

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(position: Int): Any {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return (getItem(position) as EditableKeyValue).type
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView

        val holder: EditableKeyValueViewHolder
        val item = getItem(position) as EditableKeyValue

        if (convertView == null) {
            val inflater = baseActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.i_editable_key_value, parent, false)

            holder = EditableKeyValueViewHolder(convertView)

            setValueInputType(holder.etValue, item)
            setOnValueTouchListener(holder.etValue, position)
            holder.etValue.addTextChangedListener(KeyValueTextWatcher(holder.etValue))   //works thanks to holder.etValue.setTag(item)!

            convertView.tag = holder
        } else {
            holder = convertView.tag as EditableKeyValueViewHolder
        }

        holder.etValue.tag = item

        holder.tilFormItem.hint = item.key
        holder.etValue.setText(item.value)

        return convertView!!
    }

    private fun doChoosableTask(v: View, position: Int) {
        val editableKeyValue = getItem(position) as EditableKeyValue
        v.tag = editableKeyValue
        if (editableKeyValue.clickEventListener != null) {
            baseActivity.sendClickEvent(editableKeyValue.clickEventListener!!, v)
        }
    }

    private fun setValueInputType(etValue: EditText, item: EditableKeyValue) {
        val type = item.type
        if (type == EditableKeyValue.TYPE_BLOCKED || type == EditableKeyValue.TYPE_CHOOSABLE) {
            etValue.isFocusable = false
            etValue.inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
        } else if (type == EditableKeyValue.TYPE_EDITABLE) {
            etValue.isFocusableInTouchMode = true
            etValue.isFocusable = true
            when (item.key) {
                baseActivity.getString(R.string.prompt_phone_number) -> {
                    etValue.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_CLASS_PHONE
                }
                baseActivity.getString(R.string.prompt_age) -> {
                    etValue.inputType = InputType.TYPE_CLASS_NUMBER
                }
                else -> etValue.inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
            }
        }
    }

    private fun setOnValueTouchListener(etValue: EditText, position: Int) {
        etValue.setOnTouchListener(View.OnTouchListener { v, event ->
            val type = getItemViewType(position)
            when (type) {
                EditableKeyValue.TYPE_BLOCKED -> {
                    return@OnTouchListener true
                }
                EditableKeyValue.TYPE_EDITABLE -> {
                    return@OnTouchListener false
                }
                EditableKeyValue.TYPE_CHOOSABLE -> {
                    if (event.action == MotionEvent.ACTION_UP) {
                        doChoosableTask(v, position)
                    }
                    return@OnTouchListener true
                }
                else -> true
            }
        })
    }

    private class EditableKeyValueViewHolder(convertView: View) {

        val tilFormItem: TextInputLayout = convertView.findViewById<View>(R.id.til_form_item) as TextInputLayout
        val etValue: EditText = convertView.findViewById<View>(R.id.et_value) as EditText

    }
}

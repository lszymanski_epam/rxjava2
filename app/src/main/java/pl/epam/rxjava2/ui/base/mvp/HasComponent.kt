package pl.epam.rxjava2.ui.base.mvp

/**
 * Created by Lukasz Szymanski.
 */

/**
 * Interface representing a contract for clients that contains a component for dependency injection.
 */
interface HasComponent<out C> {

    fun getComponent(): C?

}

package pl.epam.rxjava2.ui.screens.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.a_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.base.mvp.HasComponent
import pl.epam.rxjava2.ui.screens.info.InfoActivity
import pl.epam.rxjava2.ui.screens.main.blocked.BlockedUsersFragment
import pl.epam.rxjava2.ui.screens.main.experts.ExpertsFragment
import pl.epam.rxjava2.ui.screens.main.home.HomeFragment
import pl.epam.rxjava2.ui.screens.main.invite.InvitationsFragment
import pl.epam.rxjava2.ui.screens.main.profile.ProfileFragment
import pl.epam.rxjava2.ui.screens.main.profile.data.ProfileRepository
import javax.inject.Inject

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener, HasComponent<ActivityComponent> {

    @Inject
    lateinit var profileRepository: ProfileRepository

    var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_main)

        updateActivityToolbar()
        updateNavigatoinView()

        if (savedInstanceState == null) {
            openFragment(R.id.container, HomeFragment.newInstance(), false)
        }
    }

    override fun onResume() {
        super.onResume()

        updateActivityTitle()
    }

    override fun onStop() {
        super.onStop()
        clearSubscription()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.nav_home -> updateFragmentOrIgnore(item.title.toString(), HomeFragment.newInstance())
            R.id.nav_profile -> updateFragmentOrIgnore(item.title.toString(), ProfileFragment.newInstance())
            R.id.nav_experts -> updateFragmentOrIgnore(item.title.toString(), ExpertsFragment.newInstance())
            R.id.nav_invitations -> updateFragmentOrIgnore(item.title.toString(), InvitationsFragment.newInstance())
            R.id.nav_blocked_users -> updateFragmentOrIgnore(item.title.toString(), BlockedUsersFragment.newInstance())
//            R.id.nav_settings -> SettingsActivity.openActivity(this)
            R.id.nav_about -> InfoActivity.openAboutScreen(this)
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun daggerInjection() {
        super.daggerInjection()
        activityComponent!!.inject(this)
    }

    override fun getComponent(): ActivityComponent? {
        return activityComponent
    }

    private fun updateFragmentOrIgnore(newTitle: String, newFragment: BaseFragment) {
        if (title != newTitle) {
            title = newTitle
            openFragment(R.id.container, newFragment, false)
        }
    }

    private fun updateActivityToolbar() {
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
    }

    private fun updateNavigatoinView() {
        nav_view.setNavigationItemSelectedListener(this)

        getUserProfile()
    }

    private fun updateActivityTitle() {
        val menuSize = nav_view.menu.size()
        for (i in 0 until menuSize - 1) {
            if (nav_view.menu.getItem(i).isChecked) {
                title = nav_view.menu.getItem(i).title
            }
        }
    }

    private fun updateUserProfile(appUser: AppUser) {
        val header = nav_view.getHeaderView(0)
        header.tv_user_name.text = String.format("${appUser.firstName} ${appUser.lastName}")
        header.tv_user_email.text = "${appUser.email}"
    }

    private fun getUserProfile() {
        clearSubscription()

        disposable = profileRepository.getUserProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    updateUserProfile(result)
                })
    }

    private fun clearSubscription() {
        if (disposable != null && !disposable!!.isDisposed) {
            disposable!!.dispose()
        }
    }

    companion object {

        fun openActivity(context: Context) {
            context.startActivity(getStartIntent(context))
        }

        fun getStartIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }
}

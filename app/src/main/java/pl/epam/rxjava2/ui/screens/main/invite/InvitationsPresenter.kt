package pl.epam.rxjava2.ui.screens.main.invite

import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.epam.rxjava2.R
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.api.DefaultApiObserver
import pl.epam.rxjava2.api.SwipeRefreshableApiObserver
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.Invitation
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BasePresenter
import javax.inject.Inject

/**
 * Created by Lukasz Szymanski.
 */

@InjectViewState
class InvitationsPresenter(
        component: ActivityComponent
) : BasePresenter<InvitationsView>() {

    @Inject
    protected lateinit var apiService: ApiService
    @Inject
    protected lateinit var baseActivity: BaseActivity

    init {
        component.inject(this)
    }

    fun getInvitationsReceived() {
        compositeDisposable.add(apiService.getInvitationsReceived()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: SwipeRefreshableApiObserver<MutableList<Invitation>>(baseActivity, viewState) {
                    override fun onNext(result: MutableList<Invitation>) {
                        viewState.updateInvitationsList(result)
                    }
                }))
    }

    fun getInvitationsSent() {
        compositeDisposable.add(apiService.getInvitationsSent()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: SwipeRefreshableApiObserver<MutableList<Invitation>>(baseActivity, viewState) {
                    override fun onNext(result: MutableList<Invitation>) {
                        viewState.updateInvitationsList(result)
                    }
                }))
    }

    fun rejectInvitation(invitationGuid: String) {
        compositeDisposable.add(apiService.rejectInvitation(invitationGuid)
                .andThen(apiService.getInvitationsReceived())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<MutableList<Invitation>>(baseActivity) {
                    override fun onNext(result: MutableList<Invitation>) {
                        viewState.updateInvitationsList(result)
                    }

                    override fun onComplete() {
                        super.onComplete()

                        baseActivity.showMessage(
                                R.string.dialog_string_title_success,
                                R.string.dialog_string_invitation_rejected)
                    }
                }))
    }

    fun acceptInvitation(invitationGuid: String) {
        compositeDisposable.add(apiService.acceptInvitation(invitationGuid)
                .andThen(apiService.getInvitationsReceived())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<MutableList<Invitation>>(baseActivity) {
                    override fun onNext(result: MutableList<Invitation>) {
                        viewState.updateInvitationsList(result)
                    }

                    override fun onComplete() {
                        super.onComplete()

                        baseActivity.showMessage(
                                R.string.dialog_string_title_success,
                                R.string.dialog_string_invitation_accepted)
                    }
                }))
    }

    fun cancelInvitation(invitationGuid: String) {
        compositeDisposable.add(apiService.cancelInvitation(invitationGuid)
                .andThen(apiService.getInvitationsSent())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<MutableList<Invitation>>(baseActivity) {
                    override fun onNext(result: MutableList<Invitation>) {
                        viewState.updateInvitationsList(result)
                    }

                    override fun onComplete() {
                        super.onComplete()

                        baseActivity.showMessage(
                                R.string.dialog_string_title_success,
                                R.string.dialog_string_invitation_canceled)
                    }
                }))
    }

    companion object {

        private val LOG_TAG = InvitationsPresenter::class.java.simpleName
    }
}
package pl.epam.rxjava2.ui.dialogs

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.d_skills_list.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.models.SkillCategory
import pl.epam.rxjava2.ui.adapters.SkillsAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BaseDialogFragment

/**
 * Created by Lukasz Szymanski.
 */

class SkillsListDialog : BaseDialogFragment() {

    private lateinit var adapter: SkillsAdapter
    var callback: DialogCallbackWithResult<Any>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = true
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.d_skills_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        tv_header_options.text = arguments.getString(KEY_TITLE)

        adapter = SkillsAdapter(arguments.getParcelableArrayList<SkillCategory>(KEY_ITEMS_LIST),
                activity as BaseActivity,
                this)

        rv_options.adapter = adapter
        rv_options.layoutManager = LinearLayoutManager(activity)
        rv_options.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        adapter.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        adapter.onRestoreInstanceState(savedInstanceState)
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.tv_skill_name -> {
                callback?.rightConfirmBtnOnClick(view.tag)
                dismiss()
            }
            else -> super.onClickEvent(view)
        }
    }

    companion object {

        private val KEY_TITLE = "KEY_TITLE"
        private val KEY_ITEMS_LIST = "KEY_ITEMS_LIST"

        fun newInstance(title: String, itemsList: List<SkillCategory>): SkillsListDialog {
            val args = Bundle()
            args.putString(KEY_TITLE, title)
            args.putParcelableArrayList(KEY_ITEMS_LIST, itemsList as ArrayList<SkillCategory>)
            val fragment = SkillsListDialog()
            fragment.arguments = args
            return fragment
        }
    }

}
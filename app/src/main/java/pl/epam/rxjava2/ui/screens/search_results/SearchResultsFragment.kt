package pl.epam.rxjava2.ui.screens.search_results

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.f_search_results.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.constants.BundleKeys
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.models.SearchExpertsParams
import pl.epam.rxjava2.ui.adapters.SearchResultsAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.dialogs.DialogCallback
import pl.epam.rxjava2.ui.screens.other_profile.OtherUserProfileActivity


/**
 * Created by Lukasz Szymanski.
 */
class SearchResultsFragment : BaseFragment(), SearchResultsView {

    @InjectPresenter
    lateinit var presenter: SearchResultsPresenter

    @ProvidePresenter
    fun providePresenter() = SearchResultsPresenter(component!!)

    private var component: ActivityComponent? = null
    private var searchResultsAdapter: SearchResultsAdapter? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        daggerInjection()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_search_results, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupView()
        loadSearchList()
    }

    override fun onStart() {
        super.onStart()
        presenter.subscribe()
    }

    override fun onStop() {
        super.onStop()
        presenter.unSubscribe()
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.iv_send_invitation -> onSendInvitationButtonClick(view)
            R.id.rl_search_result -> {
                val baseUser: BaseUser? = view.tag as? BaseUser
                if (baseUser != null)
                    OtherUserProfileActivity.openActivity(baseActivity!!, baseUser.userGuid)
            }
            else -> super.onClickEvent(view)
        }
    }
    override fun showSwipeRefreshIndicator(isVisible: Boolean) {
        swipe_refresh_layout.isRefreshing = isVisible
    }

    override fun appendSearchResults(searchResults: MutableList<BaseUser>) {
        searchResultsAdapter?.addAllItems(searchResults)
    }

    private fun daggerInjection() {
        component = getComponent(ActivityComponent::class.java)
    }

    private fun setupView() {
        setupAdapter()
        setupRecyclerView()
        setupRefreshLayout()
    }

    private fun setupAdapter() {
        searchResultsAdapter = SearchResultsAdapter(baseActivity!!, this)
    }

    private fun setupRecyclerView() {
        rv_search_results.adapter = searchResultsAdapter
        rv_search_results.layoutManager = LinearLayoutManager(baseActivity)
        rv_search_results.addItemDecoration(DividerItemDecoration(baseActivity, DividerItemDecoration.VERTICAL))
    }

    private fun setupRefreshLayout() {
        swipe_refresh_layout.setOnRefreshListener {
            loadSearchList()
        }
    }

    private fun loadSearchList() {
        val searchExpertsParams = getSearchExpertsParams()

        presenter.searchExperts(searchExpertsParams.minAge,
                searchExpertsParams.maxAge,
                searchExpertsParams.lastUserGuid)
    }

    private fun getSearchExpertsParams() : SearchExpertsParams =
            SearchExpertsParams(arguments.getInt(BundleKeys.EXTRA_MIN_AGE),
                    arguments.getInt(BundleKeys.EXTRA_MAX_AGE),
                    getLastUserGuid())

    private fun onSendInvitationButtonClick(view: View) {
        baseActivity?.showMessage(
                R.string.dialog_string_title_warning,
                R.string.dialog_string_send_invitation_confirmation,
                true,
                object: DialogCallback() {
                    override fun rightConfirmBtnOnClick() {
                        val userGuid: String? = view.tag as? String
                        if (userGuid != null)
                            presenter.sendInvitation(userGuid, getSearchExpertsParams())
                    }
                })
    }

    private fun getLastUserGuid(): String? {
        if (searchResultsAdapter != null && searchResultsAdapter!!.itemCount > 0) {
            return searchResultsAdapter!!
                    .getItem(searchResultsAdapter!!.itemCount - 1)
                    .userGuid
        }

        return null
    }

    companion object {

        fun newInstance(minAge: Int, maxAge: Int): SearchResultsFragment {
            val args = Bundle()
            args.putInt(BundleKeys.EXTRA_MIN_AGE, minAge)
            args.putInt(BundleKeys.EXTRA_MAX_AGE, maxAge)
            val fragment = SearchResultsFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
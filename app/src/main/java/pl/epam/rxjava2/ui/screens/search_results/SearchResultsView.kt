package pl.epam.rxjava2.ui.screens.search_results

import com.arellomobile.mvp.MvpView
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.base.mvp.SwipeRefreshableView

/**
 * Created by Lukasz Szymanski.
 */

interface SearchResultsView : MvpView, SwipeRefreshableView {

    fun appendSearchResults(searchResults: MutableList<BaseUser>)

}
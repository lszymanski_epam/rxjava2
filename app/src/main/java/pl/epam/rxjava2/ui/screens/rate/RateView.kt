package pl.epam.rxjava2.ui.screens.rate

import com.arellomobile.mvp.MvpView
import pl.epam.rxjava2.models.Article

/**
 * Created by Lukasz Szymanski.
 */

interface RateView : MvpView {

    fun updateArticles(articles: MutableList<Article>)

    fun rotateArticles()

}
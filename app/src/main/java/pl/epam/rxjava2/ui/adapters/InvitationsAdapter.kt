package pl.epam.rxjava2.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import pl.epam.rxjava2.R
import pl.epam.rxjava2.models.Invitation
import pl.epam.rxjava2.ui.base.BaseRecyclerAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BaseFragment

/**
 * Created by Lukasz Szymanski.
 */

class InvitationsAdapter(
        private val baseActivity: BaseActivity,
        private val baseFragment: BaseFragment,
        private val isRejectBtnVisible: Boolean,
        private val isAcceptBtnVisible: Boolean
) : BaseRecyclerAdapter<Invitation, InvitationsAdapter.ViewHolder>(baseActivity) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.i_invitation, parent, false))
    }

    override fun setupHolder(item: Invitation, holder: ViewHolder) {
        holder.tvName.text = String.format("%s %s", item.firstName, item.lastName)
        holder.tvAge.text = if (item.age != null) item.age.toString() else ""
        holder.tvCity.text = if (item.city != null) item.city else ""

        holder.rlInvitation.tag = item
        holder.rlInvitation.setOnClickListener {
            baseActivity.sendClickEvent(baseFragment, holder.rlInvitation)
        }

        if (isRejectBtnVisible) {
            holder.ivReject.visibility = View.VISIBLE
            holder.ivReject.tag = item.guid
            holder.ivReject.setOnClickListener {
                baseActivity.sendClickEvent(baseFragment, holder.ivReject)
            }
        } else {
            holder.ivReject.visibility = View.GONE
        }

        if (isAcceptBtnVisible) {
            holder.ivAccept.visibility = View.VISIBLE
            holder.ivAccept.tag = item.guid
            holder.ivAccept.setOnClickListener {
                baseActivity.sendClickEvent(baseFragment, holder.ivAccept)
            }
        } else {
            holder.ivAccept.visibility = View.GONE
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rlInvitation: RelativeLayout
        val tvName: TextView
        val tvAge: TextView
        val tvCity: TextView
        val ivReject: ImageView
        val ivAccept: ImageView

        init {
            rlInvitation = itemView.findViewById<RelativeLayout>(R.id.rl_invitation)
            tvName = itemView.findViewById<TextView>(R.id.tv_full_name)
            tvAge = itemView.findViewById<TextView>(R.id.tv_age)
            tvCity = itemView.findViewById<TextView>(R.id.tv_city)
            ivReject = itemView.findViewById<ImageView>(R.id.iv_reject)
            ivAccept = itemView.findViewById<ImageView>(R.id.iv_accept)
        }
    }
}

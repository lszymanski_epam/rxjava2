package pl.epam.rxjava2.ui.screens.article

import com.arellomobile.mvp.InjectViewState
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.ui.base.mvp.BasePresenter

/**
 * Created by Lukasz Szymanski.
 */

@InjectViewState
class ArticlePresenter(
        component: ActivityComponent
) : BasePresenter<ArticleView>() {

    init {
        component.inject(this)
    }

    companion object {

        private val LOG_TAG = ArticlePresenter::class.java.simpleName
    }
}

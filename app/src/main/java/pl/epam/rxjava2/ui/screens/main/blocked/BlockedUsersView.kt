package pl.epam.rxjava2.ui.screens.main.blocked

import com.arellomobile.mvp.MvpView
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.base.mvp.SwipeRefreshableView

/**
 * Created by Lukasz Szymanski.
 */

interface BlockedUsersView : MvpView, SwipeRefreshableView {

    fun updateBlockedUsersList(blockedUsers: MutableList<BaseUser>)

}
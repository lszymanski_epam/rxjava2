package pl.epam.rxjava2.ui.screens.main.blocked

import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.epam.rxjava2.R
import pl.epam.rxjava2.api.*
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BasePresenter
import javax.inject.Inject

/**
 * Created by Lukasz Szymanski.
 */

@InjectViewState
class BlockedUsersPresenter (
        component: ActivityComponent
) : BasePresenter<BlockedUsersView>() {

    @Inject
    protected lateinit var apiService: ApiService
    @Inject
    protected lateinit var baseActivity: BaseActivity

    init {
        component.inject(this)
    }

    fun getBlockedUsersList() {
        compositeDisposable.add(apiService.getBlockedUsersList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: SwipeRefreshableApiObserver<MutableList<BaseUser>>(baseActivity, viewState) {
                    override fun onNext(result: MutableList<BaseUser>) {
                        viewState.updateBlockedUsersList(result)
                    }
                }))
    }

    fun unblockUser(userId: String) {
        compositeDisposable.add(apiService.unblockUser(userId)
                .andThen(apiService.getBlockedUsersList())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<MutableList<BaseUser>>(baseActivity) {
                    override fun onNext(result: MutableList<BaseUser>) {
                        viewState.updateBlockedUsersList(result)
                    }

                    override fun onComplete() {
                        super.onComplete()

                        baseActivity.showMessage(
                                R.string.dialog_string_title_success,
                                R.string.dialog_string_user_unblocked)
                    }
                }))
    }

    companion object {

        private val LOG_TAG = BlockedUsersPresenter::class.java.simpleName
    }
}
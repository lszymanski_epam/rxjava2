package pl.epam.rxjava2.ui.screens.search_results

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import pl.epam.rxjava2.R
import pl.epam.rxjava2.constants.BundleKeys
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.HasComponent

/**
 * Created by Lukasz Szymanski.
 */

class SearchResultsActivity : BaseActivity(), HasComponent<ActivityComponent> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_container)

        if (intent.extras != null) {
            val minAge = intent.extras.getInt(BundleKeys.EXTRA_MIN_AGE)
            val maxAge = intent.extras.getInt(BundleKeys.EXTRA_MAX_AGE)
            openFragment(R.id.container, SearchResultsFragment.newInstance(minAge, maxAge), false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getComponent(): ActivityComponent? {
        return activityComponent
    }

    companion object {

        fun openActivity(context: Context, minAge: Int, maxAge: Int) {
            context.startActivity(getStartIntent(context, minAge, maxAge))
        }

        fun getStartIntent(context: Context, minAge: Int, maxAge: Int): Intent {
            val intent = Intent(context, SearchResultsActivity::class.java)
            intent.putExtra(BundleKeys.EXTRA_MIN_AGE, minAge)
            intent.putExtra(BundleKeys.EXTRA_MAX_AGE, maxAge)
            return intent
        }
    }
}
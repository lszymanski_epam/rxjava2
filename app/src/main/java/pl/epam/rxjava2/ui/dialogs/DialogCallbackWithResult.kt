package pl.epam.rxjava2.ui.dialogs

/**
 * Created by Lukasz Szymanski.
 */
open class DialogCallbackWithResult<T> {

    open fun rightConfirmBtnOnClick(result: T) {

    }

    open fun leftBtnOnClick(result: T) {

    }

}

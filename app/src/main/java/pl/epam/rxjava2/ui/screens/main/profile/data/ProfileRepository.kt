package pl.epam.rxjava2.ui.screens.main.profile.data

import io.reactivex.Single
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.ui.base.BaseRepository
import java.util.Calendar
import java.util.concurrent.TimeUnit

/**
 * Created by Lukasz Szymanski.
 */

class ProfileRepository(
        private val apiService: ApiService
): BaseRepository() {

    override val expirationTimeInMillis = TimeUnit.MINUTES.toMillis(5)
    private var cachedAppUser: EntityState<AppUser>? = null

    fun getUserProfile(): Single<AppUser> {
        if (cachedAppUser != null && ifNotExpired(cachedAppUser!!.timestamp)) {
            return Single.just(cachedAppUser!!.value)
        }

        cachedAppUser = null
        return apiService.getUserProfile()
    }

    fun initCachedUserProfile(appUser: AppUser, forced: Boolean) {
        if (forced || cachedAppUser == null)
            cachedAppUser = EntityState(appUser, Calendar.getInstance().timeInMillis)
    }
}
package pl.epam.rxjava2.ui.screens.main.experts

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.f_experts.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.adapters.ExpertsAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.dialogs.DialogCallback
import pl.epam.rxjava2.ui.screens.other_profile.OtherUserProfileActivity
import pl.epam.rxjava2.ui.screens.rate.RateActivity

/**
 * Created by Lukasz Szymanski.
 */

class ExpertsFragment : BaseFragment(), ExpertsView {

    @InjectPresenter
    lateinit var presenter: ExpertsPresenter

    @ProvidePresenter
    fun providePresenter() = ExpertsPresenter(component!!)

    private var component: ActivityComponent? = null
    private var expertsAdapter: ExpertsAdapter? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        daggerInjection()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_experts, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupView()
        presenter.subscribe()

        presenter.getExpertsList(getLastExpertGuid())
    }

    override fun onDestroy() {
        presenter.unSubscribe()
        super.onDestroy()
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.iv_remove -> {
                val expert : BaseUser? = view.tag as? BaseUser
                onExpertRemovedButtonClick(expert)
            }
            R.id.iv_rate -> {
                val expert : BaseUser? = view.tag as? BaseUser
                onRateExpertButtonClick(expert)
            }
            R.id.rl_expert -> {
                val expert : BaseUser? = view.tag as? BaseUser
                if (expert != null)
                    OtherUserProfileActivity.openActivity(baseActivity!!, expert.userGuid)
            }
            else -> super.onClickEvent(view)
        }
    }

    override fun showSwipeRefreshIndicator(isVisible: Boolean) {
        swipe_refresh_layout.isRefreshing = isVisible
    }

    override fun appendExpertsList(experts: MutableList<BaseUser>) {
        expertsAdapter?.addAllItems(experts)
    }

    override fun removeExpertFromAdapter(userGuid: String) {
        val itemCount = expertsAdapter?.itemCount ?: 0
        for (i in 0..itemCount) {
            val item = expertsAdapter!!.getItem(i)
            if (item.userGuid == userGuid) {
                expertsAdapter!!.removeItem(item)
                break
            }
        }
    }

    private fun daggerInjection() {
        component = getComponent(ActivityComponent::class.java)
    }

    private fun setupView() {
        setupAdapter()
        setupRecyclerView()
        setupRefreshLayout()
    }

    private fun setupAdapter() {
        expertsAdapter = ExpertsAdapter(baseActivity!!, this)
    }

    private fun setupRecyclerView() {
        rv_experts.adapter = expertsAdapter
        rv_experts.layoutManager = LinearLayoutManager(baseActivity)
        rv_experts.addItemDecoration(DividerItemDecoration(baseActivity, DividerItemDecoration.VERTICAL))
    }

    private fun setupRefreshLayout() {
        swipe_refresh_layout.setOnRefreshListener {
            presenter.getExpertsList(getLastExpertGuid())
        }
    }

    private fun onExpertRemovedButtonClick(expert : BaseUser?) {
        if (expert != null) {
            baseActivity?.showMessage(
                    R.string.dialog_string_title_warning,
                    R.string.dialog_string_remove_expert_confirmation,
                    true,
                    object : DialogCallback() {
                        override fun rightConfirmBtnOnClick() {
                            presenter.removeExpert(expert.userGuid)
                        }
                    })
        }
    }

    private fun onRateExpertButtonClick(expert: BaseUser?) {
        if (expert != null) {
            RateActivity.openActivity(baseActivity!!, expert)
        }
    }

    private fun getLastExpertGuid(): String? {
        if (expertsAdapter != null && expertsAdapter!!.itemCount > 0) {
            return expertsAdapter!!
                    .getItem(expertsAdapter!!.itemCount - 1)
                    .userGuid
        }

        return null
    }
    companion object {

        fun newInstance(): ExpertsFragment {
            return ExpertsFragment()
        }
    }
}
package pl.epam.rxjava2.ui.screens.main.invite.received

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.f_invitations_sent.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.Invitation
import pl.epam.rxjava2.ui.adapters.InvitationsAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.screens.main.invite.InvitationsPresenter
import pl.epam.rxjava2.ui.screens.main.invite.InvitationsView
import pl.epam.rxjava2.ui.screens.other_profile.OtherUserProfileActivity

/**
 * Created by Lukasz Szymanski.
 */

class InvitationsReceivedFragment : BaseFragment(), InvitationsView {

    @InjectPresenter
    lateinit var presenter: InvitationsPresenter

    @ProvidePresenter
    fun providePresenter() = InvitationsPresenter(component!!)

    private var component: ActivityComponent? = null
    private var invitationsAdapter: InvitationsAdapter? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        daggerInjection()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_invitations_received, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupView()
    }

    override fun onStart() {
        super.onStart()
        presenter.subscribe()

        presenter.getInvitationsReceived()
    }

    override fun onStop() {
        super.onStop()
        presenter.unSubscribe()
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.iv_reject -> {
                val invitationGuid: String? = view.tag as? String
                if (!invitationGuid.isNullOrEmpty()) {
                    presenter.rejectInvitation(invitationGuid!!)
                }
            }
            R.id.iv_accept -> {
                val invitationGuid: String? = view.tag as? String
                if (!invitationGuid.isNullOrEmpty()) {
                    presenter.acceptInvitation(invitationGuid!!)
                }
            }
            R.id.rl_invitation -> {
                val invitation : Invitation? = view.tag as? Invitation
                if (invitation != null) {
                    OtherUserProfileActivity.openActivity(baseActivity!!, invitation.userGuid)
                }
            }
            else -> super.onClickEvent(view)
        }
    }

    override fun showSwipeRefreshIndicator(isVisible: Boolean) {
        swipe_refresh_layout.isRefreshing = isVisible
    }

    override fun updateInvitationsList(invitations: MutableList<Invitation>) {
        invitationsAdapter?.setItems(invitations)
    }

    private fun daggerInjection() {
        component = getComponent(ActivityComponent::class.java)
    }

    private fun setupView() {
        setupAdapter()
        setupRecyclerView()
        setupRefreshLayout()
    }

    private fun setupAdapter() {
        invitationsAdapter = InvitationsAdapter(baseActivity!!,
                this,
                true,
                true)
    }

    private fun setupRecyclerView() {
        rv_invitations.adapter = invitationsAdapter
        rv_invitations.layoutManager = LinearLayoutManager(baseActivity)
        rv_invitations.addItemDecoration(DividerItemDecoration(baseActivity, DividerItemDecoration.VERTICAL))
    }

    private fun setupRefreshLayout() {
        swipe_refresh_layout.setOnRefreshListener {
            presenter.getInvitationsReceived()
        }
    }

    companion object {

        fun newInstance(): InvitationsReceivedFragment {
            return InvitationsReceivedFragment()
        }
    }
}
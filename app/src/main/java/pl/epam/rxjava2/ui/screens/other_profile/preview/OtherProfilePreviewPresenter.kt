package pl.epam.rxjava2.ui.screens.other_profile.preview

import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.epam.rxjava2.R
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.api.DefaultApiObserver
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.AppUser
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BasePresenter
import javax.inject.Inject

/**
 * Created by Lukasz Szymanski.
 */

@InjectViewState
class OtherProfilePreviewPresenter(
        component: ActivityComponent
) : BasePresenter<OtherProfilePreviewView>() {

    @Inject
    protected lateinit var apiService: ApiService
    @Inject
    protected lateinit var baseActivity: BaseActivity

    init {
        component.inject(this)
    }

    fun getUserProfile(userGuid: String) {
        compositeDisposable.add(apiService.getUserProfile(userGuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<AppUser>(baseActivity) {
                    override fun onNext(result: AppUser) {
                        viewState.updateOtherUserProfile(result)
                    }
                }))
    }

    fun reportUser(userGuid: String, reason: String) {
        compositeDisposable.add(apiService.reportUser(userGuid, reason)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<Unit>(baseActivity) {
                    override fun onComplete() {
                        super.onComplete()

                        baseActivity.showMessage(
                                R.string.dialog_string_title_success,
                                R.string.dialog_string_user_reported)
                    }
                }))
    }

    fun blockUser(userGuid: String) {
        compositeDisposable.add(apiService.blockUser(userGuid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<Unit>(baseActivity) {
                    override fun onComplete() {
                        super.onComplete()

                        baseActivity.showMessage(
                                R.string.dialog_string_title_success,
                                R.string.dialog_string_user_blocked)
                    }
                }))
    }

    companion object {

        private val LOG_TAG = OtherProfilePreviewPresenter::class.java.simpleName
    }
}
package pl.epam.rxjava2.ui.base

import java.util.Calendar
import java.util.concurrent.TimeUnit

/**
 * Created by Lukasz Szymanski on 08.04.2018.
 */

open class BaseRepository {
    protected open val expirationTimeInMillis = TimeUnit.MINUTES.toMillis(10)

    fun ifNotExpired(timestamp: Long) : Boolean {
        return timestamp + expirationTimeInMillis > Calendar.getInstance().timeInMillis
    }
}
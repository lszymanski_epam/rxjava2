package pl.epam.rxjava2.ui.screens.web_page

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.a_web_page.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.ui.base.mvp.BaseActivity

/**
 * Created by Lukasz Szymanski.
 */

class WebPageActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_web_page)

        setupView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun loadUrlToWebView(url: String) {
        showLoadingIndicator(true)

        web_view.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY

        web_view.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                showLoadingIndicator(false)
            }

            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                showLoadingIndicator(false)
            }
        }

        web_view.loadUrl(url)
    }

    private fun setupView() {
        val extras = intent.extras
        if (extras != null) {
            val url = extras.getString(EXTRA_WEB_PAGE_URL)
            val title = extras.getString(EXTRA_WEB_PAGE_TITLE)
            val content = extras.getString(EXTRA_WEB_PAGE_CONTENT)

            if (title != null) {
                supportActionBar?.title = title
            }

            if (!TextUtils.isEmpty(url)) {
                loadUrlToWebView(url)
            } else if (!TextUtils.isEmpty(content)) {
                web_view.loadData(content, "text/html", "UTF-8")
            }
        }

        web_view.settings.javaScriptEnabled = true
        web_view.settings.setSupportZoom(true)
        web_view.settings.builtInZoomControls = true
        web_view.settings.displayZoomControls = false;
    }

    companion object {

        private val EXTRA_WEB_PAGE_URL = "EXTRA_WEB_PAGE_URL"
        private val EXTRA_WEB_PAGE_CONTENT = "EXTRA_WEB_PAGE_CONTENT"
        private val EXTRA_WEB_PAGE_TITLE = "EXTRA_WEB_PAGE_TITLE"

        fun openActivity(context: Context, url: String?, title: String?, content: String?) {
            context.startActivity(getStartIntent(context, url, title, content))
        }

        fun getStartIntent(context: Context, url: String?, title: String?, content: String?): Intent {
            val intent = Intent(context, WebPageActivity::class.java)
            intent.putExtra(EXTRA_WEB_PAGE_URL, url)
            intent.putExtra(EXTRA_WEB_PAGE_TITLE, title)
            intent.putExtra(EXTRA_WEB_PAGE_CONTENT, content)
            return intent
        }
    }
}

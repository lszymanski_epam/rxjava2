package pl.epam.rxjava2.ui.screens.rate

import com.arellomobile.mvp.InjectViewState
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import pl.epam.rxjava2.R
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.api.DefaultApiObserver
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BasePresenter
import pl.epam.rxjava2.utils.LogWrapper
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Lukasz Szymanski.
 */

@InjectViewState
class RatePresenter(
        component: ActivityComponent
) : BasePresenter<RateView>() {

    @Inject
    protected lateinit var apiService: ApiService
    @Inject
    protected lateinit var baseActivity: BaseActivity

    private var rotateArticlesDisposable: Disposable? = null

    init {
        component.inject(this)
    }

    override fun subscribe() {
        super.subscribe()
        downloadArticles()
    }

    override fun unSubscribe() {
        super.unSubscribe()
        stopRotateArticles()
    }

    fun downloadArticles() {
        stopRotateArticles()

        compositeDisposable.add(apiService.getArticles()
                .subscribeOn(Schedulers.io())
                .delay(5, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    viewState.updateArticles(result)
                    rotateArticles()
                }, { e ->
                    LogWrapper.logDebug(LOG_TAG, "Cannot download articles: ${e.message}")
                }))
    }

    fun rateExpert(userGuid: String, agreement: Boolean, opinion: String) {
        compositeDisposable.add(apiService.rateExpert(userGuid, agreement, opinion)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DefaultApiObserver<String?>(baseActivity) {
                    override fun onNext(result: String?) {
                        if (result != null) {
                            baseActivity.showMessage(
                                    baseActivity.getString(R.string.rate_expert_opinion_about_you),
                                    result)
                        }
                    }

                    override fun onComplete() {
                        super.onComplete()

                        baseActivity.showMessage(
                                R.string.dialog_string_title_success,
                                R.string.dialog_string_expert_rate_sent)
                    }
                }))
    }

    private fun rotateArticles() {
        rotateArticlesDisposable = Observable.interval(10, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.rotateArticles()
                }, { e ->
                    LogWrapper.logDebug(LOG_TAG, "Cannot rotate articles: ${e.message}")
                    stopRotateArticles()
                })
    }

    private fun stopRotateArticles() {
        if (rotateArticlesDisposable != null && !rotateArticlesDisposable!!.isDisposed) {
            rotateArticlesDisposable!!.dispose()
        }
    }

    companion object {

        private val LOG_TAG = RatePresenter::class.java.simpleName
    }
}
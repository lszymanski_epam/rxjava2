package pl.epam.rxjava2.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import pl.epam.rxjava2.R
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.base.BaseRecyclerAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.BaseFragment

/**
 * Created by Lukasz Szymanski.
 */

class ExpertsAdapter(
        private val baseActivity: BaseActivity,
        private val baseFragment: BaseFragment
) : BaseRecyclerAdapter<BaseUser, ExpertsAdapter.ViewHolder>(baseActivity) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.i_expert, parent, false))
    }

    override fun setupHolder(item: BaseUser, holder: ViewHolder) {
        holder.tvName.text = String.format("%s %s", item.firstName, item.lastName)
        holder.tvAge.text = item.age.toString()
        holder.tvCity.text = if (item.city != null) item.city else ""

        holder.rlExpert.tag = item
        holder.rlExpert.setOnClickListener {
            baseActivity.sendClickEvent(baseFragment, holder.rlExpert)
        }

        holder.ivRemove.tag = item
        holder.ivRemove.setOnClickListener {
            baseActivity.sendClickEvent(baseFragment, holder.ivRemove)
        }

        holder.ivRate.tag = item
        holder.ivRate.setOnClickListener {
            baseActivity.sendClickEvent(baseFragment, holder.ivRate)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rlExpert: RelativeLayout
        val tvName: TextView
        val tvAge: TextView
        val tvCity: TextView
        val ivRemove: ImageView
        val ivRate: ImageView

        init {
            rlExpert = itemView.findViewById<RelativeLayout>(R.id.rl_expert)
            tvName = itemView.findViewById<TextView>(R.id.tv_full_name)
            tvAge = itemView.findViewById<TextView>(R.id.tv_age)
            tvCity = itemView.findViewById<TextView>(R.id.tv_city)
            ivRemove = itemView.findViewById<ImageView>(R.id.iv_remove)
            ivRate = itemView.findViewById<ImageView>(R.id.iv_rate)
        }
    }
}
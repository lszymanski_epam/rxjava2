package pl.epam.rxjava2.ui.screens.info.about

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.f_about.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.app.Settings
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.ui.base.mvp.BaseFragment
import pl.epam.rxjava2.ui.screens.web_page.WebPageActivity
import pl.epam.rxjava2.utils.AppUtils

class AboutFragment : BaseFragment(), AboutView {

    @InjectPresenter
    lateinit var presenter: AboutPresenter

    @ProvidePresenter
    fun providePresenter() = AboutPresenter(component!!)

    private var component: ActivityComponent? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        daggerInjection()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_about, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupView()
    }

    override fun onStart() {
        super.onStart()
        presenter.subscribe()
    }

    override fun onStop() {
        super.onStop()
        presenter.unSubscribe()
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.tv_about_company -> openWebPage(Settings.COMPANY_URL, getString(R.string.app_about_company))
            else -> super.onClickEvent(view)
        }
    }

    private fun daggerInjection() {
        component = getComponent(ActivityComponent::class.java)
    }

    private fun setupView() {
        baseActivity?.supportActionBar?.show()
        baseActivity?.supportActionBar?.setTitle(R.string.about)

        val versionText = String.format("%s: %s",
                resources.getString(R.string.app_version),
                AppUtils.getAppVersion(baseActivity!!))
        tv_app_version!!.text = versionText

        tv_about_company.setOnClickListener {
            baseActivity?.sendClickEvent(this, tv_about_company)
        }
    }

    private fun openWebPage(url: String, title: String) {
        WebPageActivity.openActivity(baseActivity!!, url, title, null)
    }

    companion object {

        fun newInstance(): AboutFragment {
            return AboutFragment()
        }
    }
}

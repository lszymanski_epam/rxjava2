package pl.epam.rxjava2.ui.screens.main.invite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.f_invitations.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.ui.adapters.InvitationViewPagerAdapter
import pl.epam.rxjava2.ui.base.mvp.BaseFragment

/**
 * Created by Lukasz Szymanski.
 */

class InvitationsFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.f_invitations, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupView()
    }

    private fun setupView() {
        vp_invitations.adapter = InvitationViewPagerAdapter(this)
    }

    companion object {

        fun newInstance(): InvitationsFragment {
            return InvitationsFragment()
        }
    }
}
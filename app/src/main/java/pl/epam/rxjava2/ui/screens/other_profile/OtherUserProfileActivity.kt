package pl.epam.rxjava2.ui.screens.other_profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import pl.epam.rxjava2.R
import pl.epam.rxjava2.constants.BundleKeys
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.HasComponent
import pl.epam.rxjava2.ui.screens.other_profile.preview.OtherProfilePreviewFragment

/**
 * Created by Lukasz Szymanski.
 */

class OtherUserProfileActivity : BaseActivity(), HasComponent<ActivityComponent> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_container)

        if (intent.extras != null) {
            val userGuid = intent.extras.getString(BundleKeys.EXTRA_USER_GUID)
            openFragment(R.id.container, OtherProfilePreviewFragment.newInstance(userGuid), false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getComponent(): ActivityComponent? {
        return activityComponent
    }

    companion object {

        fun openActivity(context: Context, userGuid: String) {
            context.startActivity(getStartIntent(context, userGuid))
        }

        fun getStartIntent(context: Context, userGuid: String): Intent {
            val intent = Intent(context, OtherUserProfileActivity::class.java)
            intent.putExtra(BundleKeys.EXTRA_USER_GUID, userGuid)
            return intent
        }
    }
}
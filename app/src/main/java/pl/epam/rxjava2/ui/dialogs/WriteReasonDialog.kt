package pl.epam.rxjava2.ui.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.d_write_reason.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.ui.base.mvp.BaseDialogFragment

/**
 * Created by Lukasz Szymanski.
 */
class WriteReasonDialog : BaseDialogFragment() {

    var callback: DialogCallbackWithResult<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = true
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.d_write_reason, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        tv_dialog_title.text = arguments.getString(KEY_TITLE)

        tv_left_btn.setOnClickListener {
            sendClickEvent(this, tv_left_btn)
        }

        tv_right_btn.setOnClickListener {
            sendClickEvent(this, tv_right_btn)
        }
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.tv_left_btn -> {
                callback?.leftBtnOnClick(et_reason.text.toString())
                dialog.dismiss()
            }
            R.id.tv_right_btn -> {
                callback?.rightConfirmBtnOnClick(et_reason.text.toString())
                dialog.dismiss()
            }
            else -> super.onClickEvent(view)
        }
    }

    companion object {

        val KEY_TITLE = "KEY_TITLE"

        fun newInstance(
                title: String
        ): WriteReasonDialog {
            val args = Bundle()
            args.putString(WarningDialog.KEY_TITLE, title)
            val fragment = WriteReasonDialog()
            fragment.arguments = args
            return fragment
        }
    }
}

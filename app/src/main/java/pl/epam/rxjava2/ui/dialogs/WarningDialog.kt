package pl.epam.rxjava2.ui.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.d_warning.*
import pl.epam.rxjava2.R
import pl.epam.rxjava2.ui.base.mvp.BaseDialogFragment

/**
 * Created by Lukasz Szymanski.
 */

class WarningDialog : BaseDialogFragment() {

    var callback: DialogCallback? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = arguments.getBoolean(KEY_IS_CANCELABLE, false)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.d_warning, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        tv_dialog_title.text = arguments.getString(KEY_TITLE)
        tv_message.text = arguments.getString(KEY_MESSAGE)
        if (!arguments.getBoolean(KEY_LEFT_BUTTON_VISIBLE)) {
            tv_left_btn!!.visibility = View.GONE
        }

        tv_left_btn.setOnClickListener {
            sendClickEvent(this, tv_left_btn)
        }

        tv_right_btn.setOnClickListener {
            sendClickEvent(this, tv_right_btn)
        }
    }

    override fun onClickEvent(view: View) {
        when (view.id) {
            R.id.tv_left_btn -> {
                callback?.leftBtnOnClick()
                dialog.dismiss()
            }
            R.id.tv_right_btn -> {
                callback?.rightConfirmBtnOnClick()
                dialog.dismiss()
            }
            else -> super.onClickEvent(view)
        }
    }

    companion object {

        val KEY_TITLE = "KEY_TITLE"
        val KEY_MESSAGE = "KEY_MESSAGE"
        val KEY_LEFT_BUTTON_VISIBLE = "KEY_LEFT_BUTTON_VISIBLE"
        val KEY_IS_CANCELABLE = "KEY_IS_CANCELABLE"


        fun newInstance(
                title: String,
                message: String,
                isLeftButtonVisible: Boolean,
                isCancelable: Boolean = false
        ): WarningDialog {
            val args = Bundle()
            args.putString(WarningDialog.KEY_TITLE, title)
            args.putString(WarningDialog.KEY_MESSAGE, message)
            args.putBoolean(WarningDialog.KEY_LEFT_BUTTON_VISIBLE, isLeftButtonVisible)
            args.putBoolean(WarningDialog.KEY_IS_CANCELABLE, isCancelable)
            val fragment = WarningDialog()
            fragment.arguments = args
            return fragment
        }
    }
}

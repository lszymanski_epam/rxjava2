package pl.epam.rxjava2.ui.screens.rate

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import pl.epam.rxjava2.R
import pl.epam.rxjava2.constants.BundleKeys
import pl.epam.rxjava2.di.components.ActivityComponent
import pl.epam.rxjava2.models.BaseUser
import pl.epam.rxjava2.ui.base.mvp.BaseActivity
import pl.epam.rxjava2.ui.base.mvp.HasComponent

/**
 * Created by Lukasz Szymanski.
 */

class RateActivity : BaseActivity(), HasComponent<ActivityComponent> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_container)

        if (intent.extras != null) {
            val baseUser = intent.extras.getParcelable<BaseUser>(BundleKeys.EXTRA_BASE_USER)
            openFragment(R.id.container, RateFragment.newInstance(baseUser), false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getComponent(): ActivityComponent? {
        return activityComponent
    }

    companion object {

        fun openActivity(context: Context, baseUser: BaseUser) {
            context.startActivity(getStartIntent(context, baseUser))
        }

        fun getStartIntent(context: Context, baseUser: BaseUser): Intent {
            val intent = Intent(context, RateActivity::class.java)
            intent.putExtra(BundleKeys.EXTRA_BASE_USER, baseUser)
            return intent
        }
    }
}
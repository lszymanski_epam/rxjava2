package pl.epam.rxjava2.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by Lukasz Szymanski.
 */

data class Skill(

        @SerializedName("Id")
        val id: Int,

        @SerializedName("Name")
        var name: String,

        @SerializedName("CategoryId")
        var categoryId: Int,

        @SerializedName("IsMain")
        var isMain: Boolean = false

) : Parcelable {

        constructor(source: Parcel) : this(
                source.readInt(),
                source.readString(),
                source.readInt(),
                source.readByte().equals(1)
        )

        override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeInt(id)
                dest.writeString(name)
                dest.writeInt(categoryId)
                dest.writeByte(if (isMain) 1 else 0)
        }

        override fun describeContents() = 0


        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<Skill> = object : Parcelable.Creator<Skill> {

                        override fun createFromParcel(source: Parcel): Skill =
                                Skill(source)

                        override fun newArray(size: Int): Array<Skill?> =
                                arrayOfNulls(size)
                }
        }
}
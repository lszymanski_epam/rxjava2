package pl.epam.rxjava2.models

/**
 * Created by Lukasz Szymanski.
 */

open class KeyValue(var key: String, var value: String)
package pl.epam.rxjava2.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Lukasz Szymanski.
 */

data class Invitation (

        @SerializedName("InvitationId")
        val guid: String = "",

        @SerializedName("UserId")
        val userGuid: String = "",

        @SerializedName("FirstName")
        var firstName: String? = "",

        @SerializedName("LastName")
        var lastName: String? = "",

        @SerializedName("City")
        var city: String? = "",

        @SerializedName("Age")
        var age: Int = 0

)
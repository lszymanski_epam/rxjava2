package pl.epam.rxjava2.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Lukasz Szymanski.
 */

data class SearchExpertsParams(

        @SerializedName("MinAge")
        var minAge: Int = 18,

        @SerializedName("MaxAge")
        var maxAge: Int = 60,

        @SerializedName("LastUserGuid")
        var lastUserGuid: String? = null

)
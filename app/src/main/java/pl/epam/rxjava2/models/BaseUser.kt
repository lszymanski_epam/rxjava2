package pl.epam.rxjava2.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by Lukasz Szymanski.
 */

data class BaseUser(

        @SerializedName("UserId")
        val userGuid: String,

        @SerializedName("FirstName")
        var firstName: String? = "",

        @SerializedName("LastName")
        var lastName: String? = "",

        @SerializedName("City")
        var city: String? = "",

        @SerializedName("Age")
        var age: Int = 0

) : Parcelable {

        constructor(source: Parcel) : this (
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString(),
                source.readInt()
        )

        override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeString(userGuid)
                dest.writeString(firstName)
                dest.writeString(lastName)
                dest.writeString(city)
                dest.writeInt(age)
        }

        override fun describeContents() = 0


        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<BaseUser> = object : Parcelable.Creator<BaseUser> {

                        override fun createFromParcel(source: Parcel): BaseUser =
                                BaseUser(source)

                        override fun newArray(size: Int): Array<BaseUser?> =
                                arrayOfNulls(size)
                }
        }
}
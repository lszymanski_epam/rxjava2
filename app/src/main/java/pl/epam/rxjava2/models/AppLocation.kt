package pl.epam.rxjava2.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Lukasz Szymanski.
 */

data class AppLocation(

        @SerializedName("Lat")
        var latitude: Double? = null,

        @SerializedName("Lng")
        var longitude: Double? = null

)
package pl.epam.rxjava2.models


import com.google.gson.annotations.SerializedName;

data class AppUser(

        @SerializedName("UserId")
        val guid: String,

        @SerializedName("Mail")
        var email: String? = "",

        @SerializedName("FirstName")
        var firstName: String? = "",

        @SerializedName("LastName")
        var lastName: String? = "",

        @SerializedName("Phone")
        var phone: String? = "",

        @SerializedName("City")
        var city: String? = "",

        @SerializedName("Age")
        var age: Int = 0,

        @SerializedName("Skills")
        var skills: MutableList<Skill> = mutableListOf()

)
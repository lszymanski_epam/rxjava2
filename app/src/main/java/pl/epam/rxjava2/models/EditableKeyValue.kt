package pl.epam.rxjava2.models

import pl.epam.rxjava2.listeners.ClickEventListener

/**
 * Created by Lukasz Szymanski.
 */

class EditableKeyValue : KeyValue {

    var type: Int = TYPE_EDITABLE
    var clickEventListener: ClickEventListener? = null
    var tag: Any? = null

    constructor(key: String,
                value: String,
                type: Int = TYPE_EDITABLE,
                clickEventListener: ClickEventListener? = null,
                tag: Any? = null) : super(key, value) {

        this.type = type
        this.clickEventListener = clickEventListener
        this.tag = tag
    }

    companion object {

        val TYPE_EDITABLE = 0
        val TYPE_CHOOSABLE = 1
        val TYPE_BLOCKED = 2
    }
}

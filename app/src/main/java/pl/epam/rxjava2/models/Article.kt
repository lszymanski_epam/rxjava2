package pl.epam.rxjava2.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by Lukasz Szymanski.
 */

data class Article(

        @SerializedName("Title")
        val title: String = "",

        @SerializedName("ImageUrl")
        var imageUrl: String = "",

        @SerializedName("Website")
        var website: String = "",

        @SerializedName("Source")
        var source: String = ""

) : Parcelable {

        constructor(source: Parcel) : this (
                source.readString(),
                source.readString(),
                source.readString(),
                source.readString()
        )

        override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeString(title)
                dest.writeString(imageUrl)
                dest.writeString(website)
                dest.writeString(source)
        }

        override fun describeContents() = 0


        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<Article> = object : Parcelable.Creator<Article> {

                        override fun createFromParcel(source: Parcel): Article =
                                Article(source)

                        override fun newArray(size: Int): Array<Article?> =
                                arrayOfNulls(size)
                }
        }
}
package pl.epam.rxjava2.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

/**
 * Created by Lukasz Szymanski.
 */

data class SkillCategory(

        @SerializedName("Id")
        val id: Int,

        @SerializedName("Name")
        var name: String?,

        var skills: MutableList<Skill> = mutableListOf()

) : ExpandableGroup<Skill>(name, skills), Parcelable {

        override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeInt(id)
                dest.writeString(name)
                dest.writeTypedList(skills)
        }

        override fun describeContents() = 0


        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<SkillCategory> = object : Parcelable.Creator<SkillCategory> {

                        override fun createFromParcel(source: Parcel): SkillCategory {
                                val parceledId = source.readInt()
                                val parceledName = source.readString()
                                val parceledSkills = mutableListOf<Skill>()
                                source.readTypedList(parceledSkills, Skill.CREATOR)

                                return SkillCategory(parceledId, parceledName, parceledSkills)
                        }

                        override fun newArray(size: Int): Array<SkillCategory?> =
                                arrayOfNulls(size)
                }
        }
}
package pl.epam.rxjava2.constants


object RestMethodNames {

    //Articles
    const val ARTICLES = "api/1.0/Articles"

    //Blocked
    const val USER_BLOCKED_LIST = "api/1.0/Blocked"
    const val USER_BLOCK = "api/1.0/Blocked/Add/{${RestMethodParams.USER_ID}}"
    const val USER_UNBLOCK = "api/1.0/Blocked/Remove/{${RestMethodParams.USER_ID}}"

    //Experts
    const val EXPERTS_LIST = "api/1.0/Experts"
    const val EXPERTS_REMOVE = "api/1.0/Experts/Delete/{${RestMethodParams.USER_ID}}"
    const val EXPERTS_SEARCH = "api/1.0/Experts/Search/{${RestMethodParams.DISTANCE}}/{${RestMethodParams.MIN_AGE}}/{${RestMethodParams.MAX_AGE}}"
    const val EXPERTS_RATE = "api/1.0/Experts/Rate/{${RestMethodParams.USER_ID}}/{${RestMethodParams.AGREEMENT}}"

    //Invitations
    const val INVITATIONS_ACCEPT = "api/1.0/Invitations/Accept/{${RestMethodParams.INVITATION_ID}}"
    const val INVITATIONS_CANCEL = "api/1.0/Invitations/Cancel/{${RestMethodParams.INVITATION_ID}}"
    const val INVITATIONS_INVITE = "api/1.0/Invitations/Invite/{${RestMethodParams.USER_ID}}"
    const val INVITATIONS_REJECT = "api/1.0/Invitations/Reject/{${RestMethodParams.INVITATION_ID}}"
    const val INVITATIONS_RECEIVED = "api/1.0/Invitations/Received"
    const val INVITATIONS_SENT = "api/1.0/Invitations/Sent"

    //Report
    const val REPORT_OTHER_USER = "api/1.0/Report/UserProfile/{${RestMethodParams.USER_ID}}"

    //Skills
    const val SKILLS_ALL = "api/1.0/Skills"
    const val SKILLS_CATEGORIES_ALL = "api/1.0/Skills/AllCategories"

    //User
    const val USER_PROFILE = "api/1.0/User"
    const val USER_PROFILE_OTHER = "api/1.0/User/{${RestMethodParams.USER_ID}}"
    const val USER_PROFILE_UPDATE = "api/1.0/User/UpdateData"
}

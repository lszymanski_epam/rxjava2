package pl.epam.rxjava2.constants


object BundleKeys {

    const val EXTRA_USER_GUID = "EXTRA_USER_GUID"
    const val EXTRA_BASE_USER = "EXTRA_BASE_USER"
    const val EXTRA_MIN_AGE = "EXTRA_MIN_AGE"
    const val EXTRA_MAX_AGE = "EXTRA_MAX_AGE"
    const val EXTRA_ARTICLE = "EXTRA_ARTICLE"

}

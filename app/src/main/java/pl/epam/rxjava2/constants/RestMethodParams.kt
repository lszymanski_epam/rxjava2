package pl.epam.rxjava2.constants


object RestMethodParams {
    const val USER_ID = "userId"
    const val INVITATION_ID = "invitationId"
    const val DISTANCE = "distance"
    const val MIN_AGE = "minAge"
    const val MAX_AGE = "maxAge"
    const val AMOUNT_OF_USERS = "amountOfUsers"
    const val LAST_USER_GUID = "lastUserGuid"
    const val AGREEMENT = "agreement"
}

package pl.epam.rxjava2.listeners

import android.view.View

/**
 * Created by Lukasz Szymanski.
 */
interface ClickEventListener {

    fun onClickEvent(view: View)

}
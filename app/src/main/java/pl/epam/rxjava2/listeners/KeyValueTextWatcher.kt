package pl.epam.rxjava2.listeners

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import pl.epam.rxjava2.models.KeyValue

/**
 * Created by Lukasz Szymanski.
 */

class KeyValueTextWatcher(val view: EditText) : TextWatcher {

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(s: Editable) {
        val item = view.tag as KeyValue
        item.value = s.toString()
    }
}
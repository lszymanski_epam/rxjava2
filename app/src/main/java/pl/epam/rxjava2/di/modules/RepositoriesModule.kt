package pl.epam.rxjava2.di.modules

import dagger.Module
import dagger.Provides
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.di.scopes.AppScope
import pl.epam.rxjava2.ui.screens.main.profile.data.ProfileRepository
import pl.epam.rxjava2.ui.screens.main.profile.data.SkillsRepository

/**
 * Created by Lukasz Szymanski.
 */

@Module
class RepositoriesModule {

    var profileRepository: ProfileRepository? = null
    var skillsRepository: SkillsRepository? = null

    @Provides
    @AppScope
    fun provideProfileRepository(apiService: ApiService): ProfileRepository {
        if (profileRepository == null)
            profileRepository = ProfileRepository(apiService)
        return profileRepository!!
    }

    @Provides
    @AppScope
    fun provideSkillsRepository(apiService: ApiService): SkillsRepository {
        if (skillsRepository == null)
            skillsRepository = SkillsRepository(apiService)
        return skillsRepository!!
    }

}
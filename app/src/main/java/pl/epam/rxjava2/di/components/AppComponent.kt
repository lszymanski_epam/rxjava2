package pl.epam.rxjava2.di.components

import dagger.Component
import pl.epam.rxjava2.api.ApiService
import pl.epam.rxjava2.app.App
import pl.epam.rxjava2.app.prefs.SharedPreferencesHelper
import pl.epam.rxjava2.di.modules.ApiModule
import pl.epam.rxjava2.di.modules.AppModule
import pl.epam.rxjava2.di.modules.ConstantsModule
import pl.epam.rxjava2.di.modules.PrefsModule
import pl.epam.rxjava2.di.modules.RepositoriesModule
import pl.epam.rxjava2.di.scopes.AppScope
import pl.epam.rxjava2.ui.screens.main.profile.data.ProfileRepository
import pl.epam.rxjava2.ui.screens.main.profile.data.SkillsRepository

/**
 * Created by Lukasz Szymanski.
 */

@AppScope
@Component(modules = arrayOf(AppModule::class, ConstantsModule::class, ApiModule::class, PrefsModule::class, RepositoriesModule::class))
interface AppComponent {

    fun getApplication(): App

    fun getApiService(): ApiService

    fun getSharedPreferencesHelper(): SharedPreferencesHelper

    fun getProfileRepository(): ProfileRepository

    fun getSkillsRepository(): SkillsRepository

}

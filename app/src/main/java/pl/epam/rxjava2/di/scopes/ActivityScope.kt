package pl.epam.rxjava2.di.scopes

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * Created by Lukasz Szymanski.
 */

@Retention(RetentionPolicy.RUNTIME)
annotation class ActivityScope

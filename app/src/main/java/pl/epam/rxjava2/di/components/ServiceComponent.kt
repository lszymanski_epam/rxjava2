package pl.epam.rxjava2.di.components

import dagger.Component

/**
 * Created by Lukasz Szymanski.
 */

@Component(dependencies = arrayOf(AppComponent::class))
interface ServiceComponent {

    //injection classes

}

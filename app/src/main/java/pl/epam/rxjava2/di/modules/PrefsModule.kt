package pl.epam.rxjava2.di.modules

import android.content.SharedPreferences
import android.preference.PreferenceManager

import dagger.Module
import dagger.Provides
import pl.epam.rxjava2.app.App
import pl.epam.rxjava2.app.prefs.SharedPreferencesHelper
import pl.epam.rxjava2.di.scopes.AppScope

/**
 * Created by Lukasz Szymanski.
 */

@Module
class PrefsModule {

    @Provides
    @AppScope
    fun provideSharedPreferences(context: App): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Provides
    @AppScope
    internal fun provideSharedPreferencesHelper(preferences: SharedPreferences): SharedPreferencesHelper {
        return SharedPreferencesHelper(preferences)
    }

}

package pl.epam.rxjava2.di.modules

import dagger.Module
import dagger.Provides
import pl.epam.rxjava2.di.scopes.ActivityScope
import pl.epam.rxjava2.ui.base.mvp.BaseActivity

/**
 * Created by Lukasz Szymanski.
 */

@Module
class BaseActivityModule(private val activity: BaseActivity) {


    @Provides
    @ActivityScope
    internal fun provideActivity(): BaseActivity {
        return activity
    }
}

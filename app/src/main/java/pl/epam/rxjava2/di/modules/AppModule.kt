package pl.epam.rxjava2.di.modules

import dagger.Module
import dagger.Provides
import pl.epam.rxjava2.app.App
import pl.epam.rxjava2.di.scopes.AppScope

/**
 * Created by Lukasz Szymanski.
 */

@Module
class AppModule(private val mApplication: App) {

    @Provides
    @AppScope
    internal fun provideApplication(): App {
        return mApplication
    }

}

package pl.epam.rxjava2.di.components

import dagger.Component
import pl.epam.rxjava2.di.modules.BaseActivityModule
import pl.epam.rxjava2.di.scopes.ActivityScope
import pl.epam.rxjava2.ui.screens.article.ArticlePresenter
import pl.epam.rxjava2.ui.screens.info.about.AboutPresenter
import pl.epam.rxjava2.ui.screens.main.MainActivity
import pl.epam.rxjava2.ui.screens.main.blocked.BlockedUsersPresenter
import pl.epam.rxjava2.ui.screens.main.experts.ExpertsPresenter
import pl.epam.rxjava2.ui.screens.main.home.HomePresenter
import pl.epam.rxjava2.ui.screens.main.invite.InvitationsPresenter
import pl.epam.rxjava2.ui.screens.main.profile.ProfilePresenter
import pl.epam.rxjava2.ui.screens.other_profile.preview.OtherProfilePreviewPresenter
import pl.epam.rxjava2.ui.screens.rate.RatePresenter
import pl.epam.rxjava2.ui.screens.search_results.SearchResultsPresenter

/**
 * Created by Lukasz Szymanski.
 */

@ActivityScope
@Component(
        dependencies = [AppComponent::class],
        modules = [BaseActivityModule::class])
interface ActivityComponent {

    //injection classes
    fun inject(activity: MainActivity)

    fun inject(presenter: HomePresenter)

    fun inject(presenter: ProfilePresenter)

    fun inject(presenter: InvitationsPresenter)

    fun inject(presenter: ExpertsPresenter)

    fun inject(presenter: BlockedUsersPresenter)

    fun inject(presenter: OtherProfilePreviewPresenter)

    fun inject(presenter: AboutPresenter)

    fun inject(presenter: SearchResultsPresenter)

    fun inject(presenter: RatePresenter)

    fun inject(presenter: ArticlePresenter)

}
